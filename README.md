# 31392 Final Project
Morten, Fjodor, Albert, Asger

## src File Descriptions

saved to `src/`

### `basic_tracking.py`

Tracking class.

### `calibrate.py`

One stop function for calibration, first for individual cameras then for stereo. Flags for `stereoCalibrate()` may need tuning.

### `rectify.py`

Callable function to return rectification maps

* `[map_left, map_right], [roi_left, roi_right], Q = rectify(image_size, new_image_size=image_size, alpha=0)`

### `epipolar.py`

Shows images side by side in order to validate rectification

### `disparity.py`

Attempt to create disparity maps from rectified images, needs parameter tuning

### `main.py`

Runs most things, e.g. without classification

### `darknet_test.py`

Runs everything, including Darknet (YOLO) classifier.
Requires darknet to be compiled in the darknet folder. Contact the group for the changes made to the source files and assistance.

## Pickle Files

saved to `src/pickle/`

* calib_mono.pkl
  * Intrinsic calibration parameters
  * `mtx, dist, rvecs, tvecs = calib_mono.pkl`
    * `mtx = [mtx_left, mtx_right]`
    * `dist = [dist_left, dist_right]`
* calib_stereo.pkl
  * "Extrinsic" calibration parameters
  * `mtx, dist, R, T, E, F = calib_stereo.pkl`
