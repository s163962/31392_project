#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 20:19:43 2020

@author: albert
"""
# Darknet
import cv2
import numpy as np
import scipy.special
import math
from darknet import darknet
import time
import os

# Tracker
from src.rectify import rectify
from src.basic_tracking import ObjectTracker
from src.kalman import kalman
from src.our_disparity import ourDisparity


def detectorFilterClasses(
    detections, validClasses, aliasClasses, size_mult=0.5, img_w=1280, img_h=720
):  # Keep only relevant objects
    filtered = []
    for i in detections:
        if (
            i[2][2] < size_mult * img_w and i[2][3] < size_mult * img_h
        ):  # Reasonable size
            if not (i[2][0] > 0.5 * img_w and i[2][3] > 0.2 * img_h):  # Weird thingy
                if i[0] in aliasClasses:  # Handle aliases
                    alias = aliasClasses[i[0]]
                    filtered.append((alias, i[1], i[2]))
                elif i[0] in validClasses:  # Handle valid classes
                    filtered.append(i)
    return filtered


def detectorCustomThresh(detections, threshBox=0.02, threshBook=0.005, threshCup=0.005):
    threshed = []
    for i in detections:
        if (
            (i[0] == "Box" and i[1] > threshBox)
            or (i[0] == "Book" and i[1] > threshBook)
            or (i[0] == "Cup" and i[1] > threshCup)
        ):  # Apply ustom thresh
            threshed.append(i)
    return threshed


def detectorGetBest(detections, prio=[]):  # Keep only relevant objects
    temp = 0
    tempPrio = 0
    best = []
    bestPrio = []
    prioExists = False
    if len(detections) > 0 and detections is not None:
        for i in detections:
            if i[0] in prio:  # Prioritise
                prioExists = True
                if i[1] >= tempPrio:  # Is better
                    tempPrio = i[1]
                    bestPrio = i
            elif i[1] >= temp:  # Not prioritise and is better
                temp = i[1]
                best = i
        if prioExists:
            #            print("Best: " + str(bestPrio))
            return [bestPrio]
        else:
            #            print("Best: " + str(best))
            return [best]
    else:
        return best


def detectorInitialise():
    print("Initialising detector")
    darknet.performDetect(
        configPath=detectorConfigPath,
        weightPath=detectorWeightPath,
        metaPath=detectorMetaPath,
        initOnly=True,
    )  # Only initialize globals. Don't actually run a prediction.


#    print("Done initialising")


def detectorDetect(imageRGB, timing=False):
    ### Do detection
    prev_time = time.time()  # Time
    if timing:
        print("Performing detection of RGB image")
    detections = darknet.performDetectAS(
        imageInRGB=imageRGB,  # MUST BE RGB
        thresh=detectorThresh,
        configPath=detectorConfigPath,
        weightPath=detectorWeightPath,
        metaPath=detectorMetaPath,
        showImage=False,  # Shows image using scikit - halts execution
        makeImageOnly=False,  # If showImage is True, this won't actually *show* the image, but will create the array and return it.
        initOnly=False,
    )  # Only initialize globals. Don't actually run a prediction.
    if timing:
        print("Detected in " + str((time.time() - prev_time) * 1000) + " ms")
    return detections


def detectorDetectBGR(imageBGR, timing=False):
    ### Do detection
    imageRGB = cv2.cvtColor(imageBGR, cv2.COLOR_BGR2RGB)
    prev_time = time.time()  # Time
    if timing:
        print("Performing detection of BGR image")
    detections = darknet.performDetectAS(
        imageInRGB=imageRGB,  # MUST BE RGB
        thresh=detectorThresh,
        configPath=detectorConfigPath,
        weightPath=detectorWeightPath,
        metaPath=detectorMetaPath,
        showImage=False,  # Shows image using scikit - halts execution
        makeImageOnly=False,  # If showImage is True, this won't actually *show* the image, but will create the array and return it.
        initOnly=False,
    )  # Only initialize globals. Don't actually run a prediction.
    if timing:
        print("Detected in " + str((time.time() - prev_time) * 1000) + " ms")
    return detections


def detectorDetectFile(
    detectorFilePath="data/course/sample_stereo_conveyor_without_occlusions/left/left_0042.png",
    timing=False,
):
    prev_time = time.time()  # Time
    print("Performing detection of " + detectorFilePath)
    detections = darknet.performDetect(
        imagePath=detectorFilePath,
        thresh=detectorThresh,
        configPath=detectorConfigPath,
        weightPath=detectorWeightPath,
        metaPath=detectorMetaPath,
        showImage=False,  # Shows image using scikit - halts execution
        makeImageOnly=False,  # If showImage is True, this won't actually *show* the image, but will create the array and return it.
        initOnly=False,
    )  # Only initialize globals. Don't actually run a prediction.
    if timing:
        print("Detected in " + str((time.time() - prev_time) * 1000) + " ms")
    return detections


def drawObject(img, text, color, x, y, w, h):
    # Function to draw bounding box on image
    # x, y is center coordinates
    thickness_box = 4
    thickness_text = 2

    # Define points
    x0 = int(x - w / 2)  # top left
    y0 = int(y - h / 2)
    x1 = int(x + w / 2)  # bottom right
    y1 = int(y + h / 2)

    # Draw
    cv2.rectangle(img, (x0, y0), (x1, y1), (255, 0, 0), thickness_box)
    cv2.putText(
        img, text, (x0, y0 - 5), cv2.FONT_HERSHEY_PLAIN, 1, color, thickness_text
    )

    return img


def cap_rect(cap, map):
    ret, img = cap.read()
    if ret:
        rect = cv2.remap(img, map[0], map[1], cv2.INTER_CUBIC)
    else:
        rect = None
    return ret, rect


def get_rect(img, map):
    rect = cv2.remap(img, map[0], map[1], cv2.INTER_CUBIC)
    return rect


def draw_roi(img, roi):
    cv2.rectangle(
        img, (roi[0], roi[2]), (roi[1], roi[3]), color=(0, 255, 0), thickness=1
    )


# Flags
debug = False
writeLabel = False
continuousLoop = False
video = True

# Assuming file is being run from wdir='home/bla/bla/31392_project'
detectorConfigPath = "detector/cfg/yolov3-spp_31392.cfg"
# detectorConfigPath = "detector/cfg/yolov3-spp.cfg"
# detectorWeightPath = "detector/weights/backup/yolov3-spp_31392-train_501000.weights"
# detectorWeightPath = "detector/weights/backup/yolov3-spp_31392-train_last.weights"
# detectorWeightPath = "detector/weights/backup/yolov3-spp_31392-train_best.weights" # Still shit
detectorWeightPath = "detector/weights/yolov3-spp_final.weights"
detectorMetaPath = "detector/cfg/yolo31392.data"
detectorThresh = 0.001  # Open Images weights without our own training occasionally has confidence around 0.03 on some sample boxes...

# Define classes to detect
validClasses = ["Cup", "Book", "Box"]
aliasClasses = {"Mug": "Cup", "Coffee cup": "Cup"}

# Define constants - ID, name and color
classNameID = {"Box": 269, "Book": 69, "Cup": 56}
classIDName = {269: "Box", 69: "Book", 56: "Cup"}
classIDColor = {269: (255, 0, 0), 69: (0, 255, 0), 56: (0, 0, 255)}

# Define path - could maybe be done as argument
# path = "data/train/cup/cup00001"
# path = "data/train/dist/dist00000"
# path = "data/course/Stereo_conveyor_without_occlusions/right"
# path = "data/course/Stereo_conveyor_with_occlusions/right"
# path = "data/course/rectified/Stereo_conveyor_without_occlusions/right"
# path = "data/course/rectified/Stereo_conveyor_with_occlusions/right"
# path = "data/course/rectified/Stereo_conveyor_with_occlusions"
path = "data/course/Stereo_conveyor_without_occlusions"

withOcclusion = False
if withOcclusion:
    path = "data/course/Stereo_conveyor_with_occlusions"

# Initialise queue for determining object
queue = []
queueDepthMax = 35  # Number of frames

# Parameters and initialise monitoring of objects leaving
monitorInitFrames = 10  # Number of frames to compute mean and standard deviation over
monitorInFrames = []
monitorOutFrames = []

#if withOcclusion:
#    monitorInArea = [[1100, 280], [1120, 300]]  # Define area for detecting incoming item
#    monitorOutArea = [[250, 510], [270, 530]]  # Define area for detecting outgoing item
#else:
#    monitorInArea = [[1100, 300], [1120, 320]]  # Define area for detecting incoming item
#    monitorOutArea = [[250, 510], [270, 530]]  # Define area for detecting outgoing item
    
monitorInArea = [[1100, 300], [1120, 320]]  # Define area for detecting incoming item
monitorOutArea = [[250, 510], [270, 530]]  # Define area for detecting outgoing item
monitorInMinCount = (
    0.50
    * (monitorInArea[1][0] - monitorInArea[0][0])
    * (monitorInArea[1][1] - monitorInArea[0][1])
)  # Minimum area with deviations
monitorOutMinCount = (
    0.50
    * (monitorOutArea[1][0] - monitorOutArea[0][0])
    * (monitorOutArea[1][1] - monitorOutArea[0][1])
)
monitorDeadspace = 10  # Allow for a bit more variation in pixel values
monitorInDetected = False
monitorInDetectedPrev = False
monitorOutDetected = False
monitorOutDetectedPrev = False
monitorRefreshWait = 20  # Frames to wait before refresh
monitorInLatest = 0
monitorInDoRefresh = False
monitorOutLatest = 0
monitorOutDoRefresh = False
monitorIn = False
monitorOut = False
itemOnConveyor = False
itemOnConveyorFalling = False
itemOnConveyorRising = False
itemOnConveyorRisingDelay = False
itemOnConveyorRisingDelayIndex = 0
itemOnConveyorRisingDelayAmount = 17
#if withOcclusion:
#    itemOnConveyorRisingDelayAmount = 17
#else:
#    itemOnConveyorRisingDelayAmount = 17

kalman_reset = True

# Define Region of Interest as list of polygon points
roi_poly = [
    np.array([[130, 340], [1100, 175], [1350, 320], [170, 740]], dtype=np.int32)
]

# Weights for detections
boxWeight = 1
bookWeight = 5
cupWeight = 2

# List of items
items = []
itemsConf = []
itemsDetected = []
itemCountTotal = 0
itemCountCurrent = 0

# Find content at path
content = sorted(os.listdir(path))
imgs = []
for string in content:
    if (string[-4:] == ".jpg") or (string[-4:] == ".png"):  # Choose images
        imgs.append(string)


print("Main File for Conveyor Tracking")
print("===============================")

# file_path = "../data/course/Stereo_conveyor_with_occlusions"
print(f"Loading images from {path}...")
cap_left = cv2.VideoCapture(path + "/left/left_%04d.png")
cap_right = cv2.VideoCapture(path + "/right/right_%04d.png")

frame = [[], []]
ret, frame[0] = cap_left.read()
if not ret:
    print("Left file not found")
    exit()
frame = [[], []]
ret, frame[0] = cap_right.read()
if not ret:
    print("Right file not found")
    exit()

alpha = 0
print(f"Loading rectify maps for alpha={alpha}...")
maps, _, _ = rectify(frame[0].shape[:2], alpha=alpha)
rect = get_rect(frame[0], maps[1])

print(f"Creating tracking object...")
# roi = [370, 1240, 300, 690]
roi_track = None

if "without" in path:
    box_video = False
else:
    box_video = True

tracker = ObjectTracker(
    rect, roi=roi_track, draw=True, save_video=False, box_video=box_video
)
# ret, rect = cap_rect(cap_left, maps[0])

print(f"Creating KF object...")
#A = np.array(
#    [
#        [1, 1, 0.5, 0, 0, 0],
#        [0, 1, 1, 0, 0, 0],
#        [0, 0, 1, 0, 0, 0],
#        [0, 0, 0, 1, 1, 0.5],
#        [0, 0, 0, 0, 1, 1],
#        [0, 0, 0, 0, 0, 1],
#    ]
#)
# A = np.array(
#    [
#        [1, 1, 0, 0, 0, 0],
#        [0, 1, 0, 0, 0, 0],
#        [0, 0, 0, 0, 0, 0],
#        [0, 0, 0, 1, 1, 0],
#        [0, 0, 0, 0, 1, 0],
#        [0, 0, 0, 0, 0, 0],
#    ]
# )
A = np.array(
    [
         [1, 1, 0.5, 0, 0, 0, 0, 0, 0],
         [0, 1, 1, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 1, 0.5, 0, 0, 0],
         [0, 0, 0, 0, 1, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 1, 1, 0.5],
         [0, 0, 0, 0, 0, 0, 0, 1, 1],
         [0, 0, 0, 0, 0, 0, 0, 0, 1],
    ]
)
# A = np.array([[1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 1, 1], [0, 0, 0, 1]])
# C = np.array(
#     [
#         [1, 0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 1, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 1, 0, 0],
#         #        [1, 0, 0, 0, 0, 0],
#         #        [0, 0, 0, 1, 0, 0],
#         #        [1, 0, 0, 0, 0, 0],
#         #        [0, 0, 0, 1, 0, 0],
#     ]
# )
C = np.array([[1, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0],])
# C = np.array([[1, 0, 0, 0], [0, 0, 1, 0], [1, 0, 0, 0], [0, 0, 1, 0]])

# kf = kalman(A, C, sig2_x0=1e5, sig2_Q=[1e-2, 5e-5, 1e-5, 1e-2, 5e-5, 1e-5], sig2_R=[1e0, 1e0])#, 5e2, 5e2, 5e-3, 5e-3])
# kf = kalman(
#     A, C, sig2_x0=5e-1, sig2_Q=[0, 0, 0, 0, 0, 0, 0, 0, 0], sig2_R=[1e-2, 1e-0, 1e-2]
# )
kf = kalman(
    A, C, sig2_x0=1e3, sig2_Q=[1e-1, 1e-3, 1e-1, 1e-3, 1e0, 1e-3, 1e-1, 1e-1, 1e-1], sig2_R=[5e0, 5e2, 5e0]
)
# found = False

# print(f"Begin tracking...")
mask_track = tracker.generate_mask(rect.copy())
static_img = np.zeros_like(rect, np.uint64)
loopcount = 0
mask_length = 80


# Disparity
disp = ourDisparity()
z_last = 0

### Initialise
print("--------------------- INITIALISING ---------------------")
# Initialise detector
detectorInitialise()

print("--------------------- DONE INITIALISING ---------------------")

# print("--------------------- TESTING ---------------------")
### Prep image
# detectorFilePath = "data/course/sample_stereo_conveyor_without_occlusions/left/left_0042.png"
# imBGR = cv2.imread(detectorFilePath) # BGR
# imRGB = cv2.cvtColor(imBGR, cv2.COLOR_BGR2RGB)
#
#### Do detection
## RGB image
# detections1 = detectorDetect(imRGB, timing = True)
# print(detections1)
#
## BGR image
# detections2 = detectorDetectBGR(imBGR, timing = True)
# print(detections2)
#
## File
# detections3 = detectorDetectFile(detectorFilePath, timing = True)
# print(detections3)
#
## Filter detections
# detections = detectorFilterClasses(detections1, validClasses, aliasClasses)
# print(detections)
#
# print("--------------------- DONE TESTING ---------------------")

video_path = "video/"
if video:
    # Default resolutions of the frame are obtained.The default resolutions are system dependent.
    img_dim = rect.shape  # Get dimensions
    img_h = img_dim[0]
    img_w = img_dim[1]
    img_c = img_dim[2]
    video_width = int(img_w)
    video_height = int(img_h)
    print((video_width, video_height))

    # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
    #    vid = cv2.VideoWriter('darkout.avi', cv2.VideoWriter_fourcc('M','J','P','G'), fps = 24, frameSize = (video_width, video_height), isColor=True)
    vid = cv2.VideoWriter(
        "darkout.avi",
        cv2.VideoWriter_fourcc("m", "j", "p", "g"),
        fps=24,
        frameSize=(video_width, video_height),
        isColor=True,
    )
    print("Video object created")
#    cv2.VideoWriter(
#                "left_tracker.mp4",
#                cv2.VideoWriter_fourcc("m", "p", "4", "v"),
#                24,
#                (frame.shape[1], frame[0].shape[0]),
#                isColor=True,
#            )

print("--------------------- STARTING LOOP ---------------------")
# Lets goooooooo
i = 0
kf.reset(x=[0, 0, 0, 0, 0, 0, 0, 0, 0])
while True:
    ### Input ###
    # Read image
    #    filename_img = imgs[i]
    #    path_current_img = os.path.join(path, filename_img)
    #    img_orig = cv2.imread(path_current_img)

    ret, left = cap_rect(cap_left, maps[0])
    if not ret:
        break
    ret, right = cap_rect(cap_right, maps[1])
    if not ret:
        break

    rect = right.copy()

    img_orig = rect.copy()
    img = rect.copy()


    # Get image dimensions
    img_dim = img.shape
    img_h = img_dim[0]
    img_w = img_dim[1]
    img_c = img_dim[2]

    # Read label
    #    filename_label = imgs[i][:-4] + ".txt"
    #    path_current_label = os.path.join(path, filename_label)
    #    if debug and False:
    #        print("\n" + path_current_img)

    ### Region of Interest ###
    # Create mask from ROI
    mask = np.zeros((img_h, img_w), dtype=np.uint8)
    cv2.fillPoly(mask, roi_poly, 255)
    # Mask the image for detection
    img_detect = cv2.bitwise_and(img_orig, img_orig, mask=mask)

    ### Detections ###
    # Detect and filter
    detections_all = detectorDetectBGR(img_detect, timing=False)  # Detect
    detections_filterClass = detectorFilterClasses(
        detections_all, validClasses, aliasClasses
    )  # Filter by class
    detections_filterConf = detectorCustomThresh(
        detections_filterClass
    )  # Filter by confidence
    detections_prio = detectorGetBest(
        detections_filterConf, prio=["Cup", "Book"]
    )  # Get best prioritised detection

    if debug:
        print("All detections:")
        for debug_det in detections_all:
            print(debug_det)
        print("Filtered detections:")
        for debug_det in detections_filterConf:
            print(debug_det)
        print("Prio detection:")
        for debug_det in detections_prio:
            print(debug_det)

    queueDetections = detections_prio
    drawDetections = detections_prio

    ### Determine object ###
    # Define area for monitoring object leaving
    # Refresh area
    if i == monitorInLatest + monitorRefreshWait and monitorInDoRefresh:
        if debug:
            print("Refreshing monitorIn")
        monitorInFrames = []
    if i == monitorOutLatest + monitorRefreshWait and monitorOutDoRefresh:
        if debug:
            print("Refreshing monitorOut")
        monitorOutFrames = []

    # Compute area
    if (
        len(monitorInFrames) >= 0 and len(monitorInFrames) < monitorInitFrames
    ):  # Initialise
        monitorInFrames.append(
            img_orig[
                monitorInArea[0][1] : monitorInArea[1][1],
                monitorInArea[0][0] : monitorInArea[1][0],
                :,
            ]
        )
    else:  # Check for changes
        # Compute current area parameters
        monitorInAvg = np.mean(monitorInFrames, axis=0)  # Mean
        monitorInSTD = np.std(monitorInFrames, axis=0)  # Standard deviation
        monitorInNow = img_orig[
            monitorInArea[0][1] : monitorInArea[1][1],
            monitorInArea[0][0] : monitorInArea[1][0],
            :,
        ]  # Get current area
        # Check against norm
        if (
            np.sum(monitorInNow > monitorInAvg + 3 * monitorInSTD + monitorDeadspace)
            > monitorInMinCount
            or np.sum(monitorInNow < monitorInAvg - 3 * monitorInSTD - monitorDeadspace)
            > monitorInMinCount
        ):
            monitorInDetectedPrev = monitorInDetected
            monitorInDetected = True
        else:
            monitorInDetectedPrev = monitorInDetected
            monitorInDetected = False
    # Compute area
    if (
        len(monitorOutFrames) >= 0 and len(monitorOutFrames) < monitorInitFrames
    ):  # Initialise
        monitorOutFrames.append(
            img_orig[
                monitorOutArea[0][1] : monitorOutArea[1][1],
                monitorOutArea[0][0] : monitorOutArea[1][0],
                :,
            ]
        )
    else:  # Check for changes
        # Compute current area parameters
        monitorOutAvg = np.mean(monitorOutFrames, axis=0)
        monitorOutSTD = np.std(monitorOutFrames, axis=0)
        monitorOutNow = img_orig[
            monitorOutArea[0][1] : monitorOutArea[1][1],
            monitorOutArea[0][0] : monitorOutArea[1][0],
            :,
        ]  # Get current area
        # Check against norm
        if (
            np.sum(monitorOutNow > monitorOutAvg + 3 * monitorOutSTD + monitorDeadspace)
            > monitorOutMinCount
            or np.sum(
                monitorOutNow < monitorOutAvg - 3 * monitorOutSTD - monitorDeadspace
            )
            > monitorOutMinCount
        ):
            monitorOutDetectedPrev = monitorOutDetected
            monitorOutDetected = True
        else:
            monitorOutDetectedPrev = monitorOutDetected
            monitorOutDetected = False
    # Detect rising edge for in
    if monitorInDetectedPrev == False and monitorInDetected == True:
        if debug:
            print("Object arrived at i = " + str(i))
        # Handle incoming item
        itemCountTotal += 1  # Count items passed
        itemCountCurrent += 1  # Count current items
        items.append([])  # Allocate in list
        itemsConf.append([])  # Allocate in confidence list
        itemsDetected.append(False)  # Has item been detected yet?
        monitorInRisingEdge = True
    else:
        monitorInRisingEdge = False
    # Detect falling edge for in
    if monitorInDetectedPrev == True and monitorInDetected == False:
        monitorInFallingEdge = True
        monitorInLatest = i
        monitorInDoRefresh = True
    else:
        monitorInFallingEdge = False
    # Detect falling edge for out
    if monitorOutDetectedPrev == False and monitorOutDetected == True:
        monitorOutRisingEdge = True
    else:
        monitorOutRisingEdge = False
    if monitorOutDetectedPrev == True and monitorOutDetected == False:
        if debug:
            print("Object left at i = " + str(i))
        # Handle incoming item
        itemCountCurrent -= 1  # Count current items
        if itemCountCurrent < 0:
            itemCountCurrent = 0
        monitorOutFallingEdge = True
        monitorOutLatest = i
        monitorOutDoRefresh = True
    else:
        monitorOutFallingEdge = False

    # Boolean
    if itemCountCurrent > 0:
        itemOnConveyor = True
    else:
        itemOnConveyor = False

    if itemOnConveyorRising and monitorOutRisingEdge:
        itemOnConveyorRising = False
    if itemOnConveyor and monitorInRisingEdge:
        itemOnConveyorRising = True
        itemOnConveyorRisingDelayIndex = i

    if itemOnConveyorFalling and monitorOutRisingEdge:
        itemOnConveyorFalling = False
    if itemOnConveyor and monitorInFallingEdge:
        itemOnConveyorFalling = True

    if itemOnConveyorRisingDelay and monitorOutFallingEdge:
        itemOnConveyorRisingDelay = False
    if (
        itemOnConveyorRising
        and i > itemOnConveyorRisingDelayIndex + itemOnConveyorRisingDelayAmount
    ):
        itemOnConveyorRisingDelay = True
    #    print("Count: " + str(itemCountCurrent) + "    In: " + str(monitorInDetected) + "    Out: " + str(monitorOutDetected))
    
    ### Object tracker
    if loopcount < mask_length:
        # generates a better mask
        mask_track = cv2.bitwise_or(mask_track, tracker.generate_mask(rect.copy()))
        static_img = cv2.add(np.uint64(rect), np.uint64(static_img))
    if loopcount == mask_length:
        static_img = np.uint8(static_img / mask_length)
    #        cv2.imshow("Static", static_img)

    if loopcount % 10 == 0:
        make_new_points = True
    else:
        make_new_points = False
    if loopcount >= mask_length and itemOnConveyorRisingDelay:
        box, center, moving_features, picture = tracker.track(
            rect,
            get_new_features=make_new_points,
            outlier_dist=150,
            mask=mask_track,
            static_img=static_img,
            corner=False,  # TODO fix corners if True
        )
    ### Kalman
    # Reset Kalman filter
    # Either when the item leaves or a new item enters
    if (itemCountCurrent == 1 and monitorOutFallingEdge) or monitorInRisingEdge:
        if debug:
            print("!!! KF reset, loop = " + str(loopcount))
        kf.reset(x=[0, 0, 0, 0, 0, 0, 0, 0, 0])
        kalman_reset = True
    #        found = False

    # Update Kalman filter with new measurements
    center_meas = []
    center_meas_valid = False
    if itemOnConveyorRisingDelay:
#        if queueDetections and True: # Machine learning (YOLO)
#            ml_x = queueDetections[0][2][0]
#            ml_y = queueDetections[0][2][1]
#            center_meas.append([ml_x, ml_y])
#            center_meas_valid = True
        #        if center[0] and False: # Tracking attempt 1
        #            of_x1 = center[2][0]
        #            of_y1 = center[2][1]
        #            center_meas.append([of_x1, of_y1])
        #            center_meas_valid = True
        #        if center[1] and False: # Tracking attempt 2
        #            of_x2 = center[2][0]
        #            of_y2 = center[2][1]
        #            center_meas.append([of_x2, of_y2])
        #            center_meas_valid = True
        if center[2] and True:  # Tracking attempt 3
            of_x3 = center[2][0]
            of_y3 = center[2][1]
            try:
                z = disp.getDistanceToObject(left, right, box[2])
            except:
                print("Disparity got error, using last good value")
                z = z_last
            if not math.isnan(z):
                z_last = z
            else:
                print("Disparity was NaN, using last good value")
                z = z_last
            center_meas.append([of_x3, of_y3, z])
            center_meas_valid = True
            tracker.draw_bounding_box(
                img,
                box[2],
                center[2],
                box_color=(255, 255, 0),
                center_color=(255, 255, 0),
            )
        if center_meas_valid:  # Do update
            center_meas_avg = np.mean(center_meas, axis=0)
            #        found = True
            if kalman_reset:
                print("Kalman reset")
#                kf = kalman(
#                    A, C, sig2_x0=1e3, sig2_Q=[1e-1, 1e-3, 1e-1, 1e-3, 1e0, 1e-3, 1e-1, 1e-1, 1e-1], sig2_R=[5e0, 5e2, 5e0]
#                )
#                kf.x = np.array([center_meas_avg[0], 0, 0, center_meas_avg[1], 0, 0, center_meas_avg[2], 0, 0])
#                print(kf.x)
                kf.reset(x = [center_meas_avg[0], -2, -0.05, center_meas_avg[1], 1, 0, center_meas_avg[2], 0, 0])
#                kf.reset(x = [center_meas_avg[0], 0, 0, center_meas_avg[1], 0, 0, center_meas_avg[2], 0, 0])
                kalman_reset = False
            else:
                kf.update(np.array(center_meas_avg).reshape(-1,))
            if debug:
                print(
                    "KF updated with "
                    + str(center_meas_avg)
                    + ", loop = "
                    + str(loopcount)
                )

        # Kalman predict
        kf.predict()

        # Draw Kalman mid point
        kf.draw(img, box[2])
    #    print(kf.x)

    # Continuously update confidence score for latest item
    if itemOnConveyor:
        items[itemCountTotal - 1].append(queueDetections)  # Add new
        if len(queueDetections) > 0:  # Mark detected
            itemsDetected[-1] = True
        # Running confidence of type
        runningClassCount = np.zeros((len(items), len(classNameID)))  # Box, Book, Cup
        for q in items[itemCountTotal - 1]:
            if len(q) > 0:
                if q[0][0] == "Box":
                    runningClassCount[itemCountTotal - 1][0] += (
                        boxWeight * q[0][1]
                    )  # Weighted confidence score
                if q[0][0] == "Book":
                    runningClassCount[itemCountTotal - 1][1] += (
                        bookWeight * q[0][1]
                    )  # Weighted confidence score
                if q[0][0] == "Cup":
                    runningClassCount[itemCountTotal - 1][2] += (
                        cupWeight * q[0][1]
                    )  # Weighted confidence score

        # Softmax on confidence
        runningClassConf = runningClassCount[itemCountTotal - 1]
        if np.sum(runningClassCount) > 0:
            runningClassConf = scipy.special.softmax(runningClassConf)
            itemsConf[itemCountTotal - 1].append(runningClassConf)
    else:
        queue = []

    ### Draw frame ###
    # Draw ROI
    cv2.polylines(img, roi_poly, isClosed=True, color=[170, 170, 170])

    # Draw bounding box
    img_labels = []
    # Condition labels
    for cnt, det in enumerate(drawDetections):  # Go through each line
        try:
            # Read label information

            ID = int(classNameID[det[0]])  # Class ID
            x = float(det[2][0])  # x center
            y = float(det[2][1])  # y center
            w = float(det[2][2])  # width
            h = float(det[2][3])  # height
            conf = float(det[1])

            # Add label information to list
            this_label = [ID, conf, x, y, w, h]
            img_labels.append(this_label)

        except ValueError:
            print("Something went wrong with the label")

    for bbox in img_labels:
        bbox_name = classIDName[bbox[0]]  # Get name and color form ID
        bbox_color = classIDColor[bbox[0]]
        bbox_conf = bbox[1]
        bbox_x = bbox[2]  # Absolute
        bbox_y = bbox[3]
        bbox_w = bbox[4]
        bbox_h = bbox[5]
        #        bbox_x = bbox[2] * img_w # Relative to absolute
        #        bbox_y = bbox[3] * img_h
        #        bbox_w = bbox[4] * img_w
        #        bbox_h = bbox[5] * img_h
        bbox_text = "{0} {1:.2f}%".format(bbox_name, bbox_conf * 100)
        img = drawObject(img, bbox_text, bbox_color, bbox_x, bbox_y, bbox_w, bbox_h)

    # Draw monitor area in
    if monitorInRisingEdge == True:
        monitorInColor = (255, 0, 0)  # BGR Blue
        monitorInThickness = -1  # Filled
    elif monitorInDetected == True:
        monitorInColor = (0, 255, 0)  # BGR Green
        monitorInThickness = -1  # Filled
    elif monitorInFallingEdge == True:
        monitorInColor = (0, 0, 255)  # BGR Blue
        monitorInThickness = -1  # Filled
    else:
        monitorInColor = (255, 255, 255)
        monitorInThickness = 1
    cv2.rectangle(
        img,
        (monitorInArea[0][0], monitorInArea[0][1]),
        (monitorInArea[1][0], monitorInArea[1][1]),
        monitorInColor,
        monitorInThickness,
    )
    # Draw monitor area out
    if monitorOutDetected == True:
        monitorOutColor = (0, 255, 0)  # BGR Green
        monitorOutThickness = -1  # Filled
    elif monitorOutRisingEdge == True:
        monitorOutColor = (255, 0, 0)  # BGR Blue
        monitorOutThickness = -1  # Filled
    elif monitorOutFallingEdge == True:
        monitorOutColor = (0, 0, 255)  # BGR Blue
        monitorOutThickness = -1  # Filled
    else:
        monitorOutColor = (255, 255, 255)
        monitorOutThickness = 1
    cv2.rectangle(
        img,
        (monitorOutArea[0][0], monitorOutArea[0][1]),
        (monitorOutArea[1][0], monitorOutArea[1][1]),
        monitorOutColor,
        monitorOutThickness,
    )

    # Put path on image
    if True:
        text = "Image " + str(i)
        cv2.putText(
            img, text, (20, 20), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 255), 2
        )
        text = "Kalman"
        cv2.putText(img, text, (20, 50), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "Morphology"
        cv2.putText(img, text, (20, 70), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 0), 2)
        text = "Machine Learning"
        cv2.putText(img, text, (20, 90), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 0, 0), 2)

    # Put object coordinates on image
    if kf.x is not None:
        text = "Object coordinates"
        cv2.putText(img, text, (300, 20), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "x: {:.1f}".format(kf.x[0])
        cv2.putText(img, text, (300, 50), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "y: {:.1f}".format(kf.x[3])
        cv2.putText(img, text, (300, 70), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "z: {:.1f}".format(kf.x[6])
        cv2.putText(img, text, (300, 90), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "vel x: {:.1f}".format(kf.x[1])
        cv2.putText(img, text, (450, 50), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "vel y: {:.1f}".format(kf.x[4])
        cv2.putText(img, text, (450, 70), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)
        text = "vel z: {:.1f}".format(kf.x[7])
        cv2.putText(img, text, (450, 90), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 2)

    # Put confidence on image
    if len(itemsConf) > 0:
        for k, item in enumerate(itemsConf):  # For each item seen
            if itemsDetected[k]:  # Only if detected
                conf = item[-1]  # Get confidence
                text_x = 20 + k * 150  # Offset text for each item
                textItemNr = "Item {}".format(k + 1)
                cv2.putText(
                    img,
                    textItemNr,
                    (text_x, 120),
                    cv2.FONT_HERSHEY_PLAIN,
                    1.5,
                    (255, 0, 0),
                    2,
                )
                textConf = "Box:  {:.2f}".format(conf[0])
                cv2.putText(
                    img,
                    textConf,
                    (text_x, 140),
                    cv2.FONT_HERSHEY_PLAIN,
                    1.5,
                    (255, 0, 0),
                    2,
                )
                textConf = "Book: {:.2f}".format(conf[1])
                cv2.putText(
                    img,
                    textConf,
                    (text_x, 160),
                    cv2.FONT_HERSHEY_PLAIN,
                    1.5,
                    (255, 0, 0),
                    2,
                )
                textConf = "Cup:  {:.2f}".format(conf[2])
                cv2.putText(
                    img,
                    textConf,
                    (text_x, 180),
                    cv2.FONT_HERSHEY_PLAIN,
                    1.5,
                    (255, 0, 0),
                    2,
                )
                # cv2.putText(img, textDetected, (20, 90), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 0, 0), 2)
    else:
        textConf = "No item yet"
        cv2.putText(
            img, textConf, (20, 120), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 0, 0), 2
        )

    ### Show frame ###
    # Show image with bounding box
    cv2.imshow("Detector", img)
    #    cv2.imshow("Picture", picture)

    # Write to video
    if video:
#        video_path_img = os.path.join(video_path, str(i) + ".jpg")
#        cv2.imwrite(video_path_img, img)
#        img_dim1 = img.shape  # Get dimensions
#        img_h1 = img_dim1[0]
#        img_w1 = img_dim1[1]
#        img_c1 = img_dim1[2]
#        video_width = int(img_h1)
#        video_height = int(img_w1)
#        print((video_width, video_height))
        vid.write(img)
#        print("Wrote to vid")

    # Check for keypress
    keyPressed = cv2.waitKey(1) & 0xFF  # 1 ms

    # Handle keypress
    if keyPressed == 27:  # ESC - Stop running
        cv2.destroyAllWindows()
        break

    # Handle end of videostream
    if i == len(imgs) - 1:
        if continuousLoop:  # Restart
            print("Looping...")
            i = 0
        else:  # Stop running
            #            cv2.destroyAllWindows()
            break

    loopcount = loopcount + 1
    i += 1
    # Go to next image
    if debug:
        print("Next")
    if not i >= len(imgs) - 1:
        i += 1

print("--------------------- DONE LOOPING ---------------------")
if video:
    vid.release()
    print("Release vid")

del tracker  # kinda important
cv2.destroyAllWindows()

#    if writeLabel:
#        if not os.path.exists(path_current_label):
#            if len(img_labels) > 0:
#                try:
#                    f = open(path_current_label, "w") # Open file
#                    for cnt, line in enumerate(img_labels): # Go through each line
#                        try:
#                            f.write("{} {} {} {} {}".format(line[0], line[2]*img_w, line[3]*img_h, line[4]*img_w, line[5]*img_h))
#
#                        except ValueError:
#                            print("Something went wrong writing the label")
#                        finally:
#                            f.close()
#                except ValueError:
#                    print("Couldn't open file: " + path_current_label)
#        else:
#            print("Label file exists or not accessible: " + path_current_label)
#
#    if keyPressed == ord('k'): # Left - Previous
#        if debug:
#            print("Previous")
#
#        if not i <= 0:
#            i -= 1
#
#    elif keyPressed == ord('l'): # Right - Next
#        if debug:
#            print("Next")
#
#        if not i >= len(imgs) - 1:
#            i += 1
#
#    elif keyPressed == ord('x'): # X - Delete / move
#        if debug:
#            print("Delete/move")
#
#        # Remove image from list and get filenames
#        filename_img = imgs.pop(i)
#        filename_label = filename_img[:-4] + ".txt"
#
#        # Create delete folder if it doesn't exist
#        if not os.path.exists(path_delete):
#            os.mkdir(path_delete)
#
#        # Get paths
#        path_current_img = os.path.join(path, filename_img)
#        path_current_label = os.path.join(path, filename_label)
#        path_delete_img = os.path.join(path_delete, filename_img)
#        path_delete_label = os.path.join(path_delete, filename_label)
#
#        # Move image and label
#        os.replace(path_current_img, path_delete_img)
#        os.replace(path_current_label, path_delete_label)
#        print("Moved " + filename_img)
#
#        # Correct index
#        if  i <= 0:
#            i = 0
#        elif i > len(imgs) - 1:
#            i = len(imgs) - 1
#
