import cv2 as cv2
import os

# User configurations
#itemName = "book"
#filename = [
#        "data/videos/VID_20200403_140041.mp4",
#        "data/videos/VID_20200403_135944.mp4",
#        "data/videos/VID_20200403_135858.mp4",
#        "data/videos/VID_20200403_135801.mp4",
#        "data/videos/VID_20200403_135717.mp4",
#        "data/videos/VID_20200403_135642.mp4",
#        "data/videos/VID_20200403_135606.mp4",
#        "data/videos/VID_20200403_135533.mp4",
#    ]


#itemName = "box"
#filename = [
#        "data/videos/box1_1.m4v",
#        "data/videos/box1_2.m4v",
#        "data/videos/box1_3.m4v",
#        "data/videos/box1_4.m4v",
#        "data/videos/box1_5.m4v",
#        "data/videos/box1_6.m4v",
#        "data/videos/box2_1.m4v",
#        "data/videos/box2_2.m4v",
#        "data/videos/box2_3.m4v"
#    ]

#itemName = "cup"
#filename = [
#        "data/videos/cup1_1.m4v",
#        "data/videos/cup1_2.m4v",
#        "data/videos/cup1_3.m4v",
#        "data/videos/cup1_4.m4v",
#        "data/videos/cup2_1.m4v",
#        "data/videos/cup2_2.m4v",
#        "data/videos/cup2_3.m4v",
#        "data/videos/cup3_1.m4v",
#        "data/videos/cup3_2.m4v",
#        "data/videos/cup3_3.m4v"
#    ]

itemName = "dist"
filename = [
        "data/videos/dist1.m4v"
    ]
for index in range(len(filename)):
    print(filename[index])
    itemIndex = index
    
    # it's a little hack :)
#    if itemName == "box":
#        itemIndex = 0
#        if index > 5:
#            itemIndex = 1
#    elif itemName == "cup":
#        itemIndex = 0
#        if index > 3:
#            itemIndex = 1
#        if index > 6:
#            itemIndex = 2
#    else:
#        itemIndex = index
    
    
    foldername = "{0:s}{1:05d}".format(itemName, itemIndex)#"{0:s}{1:05d}".format("book", index)
    destination = "data/temp/" + itemName + "/" + foldername + "/"
    fileprefix = ""
    
    #print(destination)
    # Import video
    vc = cv2.VideoCapture(filename[index])
    # Check and load first frame
    retval, frame = vc.read()

    if not retval:
        print("Failed opening the file ❌📽 😢")
        exit()
    i = 0

    if not os.path.exists(destination):
        os.makedirs(destination)

    # For each frame, give it a name, save it, read the next frame
    while retval:
        dest = "{0:s}{1:s}{2:05d}{3:s}".format(destination, fileprefix, i, ".jpg")
        cv2.imwrite(dest, frame)

        retval, frame = vc.read()

        i = i + 1
    # Exit
    print("Done with 📽 ➡ 📷 for file " + str(index + 1) + " of " + str(len(filename)))
    vc.release()
