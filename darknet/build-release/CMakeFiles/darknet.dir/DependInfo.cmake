# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CUDA"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/albert/project/31392_project/darknet/src/activation_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/activation_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/activations.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/activations.c.o"
  "/home/albert/project/31392_project/darknet/src/art.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/art.c.o"
  "/home/albert/project/31392_project/darknet/src/avgpool_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/avgpool_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/batchnorm_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/batchnorm_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/blas.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/blas.c.o"
  "/home/albert/project/31392_project/darknet/src/box.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/box.c.o"
  "/home/albert/project/31392_project/darknet/src/captcha.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/captcha.c.o"
  "/home/albert/project/31392_project/darknet/src/cifar.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/cifar.c.o"
  "/home/albert/project/31392_project/darknet/src/classifier.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/classifier.c.o"
  "/home/albert/project/31392_project/darknet/src/coco.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/coco.c.o"
  "/home/albert/project/31392_project/darknet/src/col2im.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/col2im.c.o"
  "/home/albert/project/31392_project/darknet/src/compare.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/compare.c.o"
  "/home/albert/project/31392_project/darknet/src/connected_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/connected_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/conv_lstm_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/conv_lstm_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/convolutional_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/convolutional_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/cost_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/cost_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/cpu_gemm.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/cpu_gemm.c.o"
  "/home/albert/project/31392_project/darknet/src/crnn_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/crnn_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/crop_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/crop_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/dark_cuda.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/dark_cuda.c.o"
  "/home/albert/project/31392_project/darknet/src/darknet.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/darknet.c.o"
  "/home/albert/project/31392_project/darknet/src/data.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/data.c.o"
  "/home/albert/project/31392_project/darknet/src/deconvolutional_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/deconvolutional_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/demo.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/demo.c.o"
  "/home/albert/project/31392_project/darknet/src/detection_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/detection_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/detector.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/detector.c.o"
  "/home/albert/project/31392_project/darknet/src/dice.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/dice.c.o"
  "/home/albert/project/31392_project/darknet/src/dropout_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/dropout_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/gaussian_yolo_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/gaussian_yolo_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/gemm.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/gemm.c.o"
  "/home/albert/project/31392_project/darknet/src/go.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/go.c.o"
  "/home/albert/project/31392_project/darknet/src/gru_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/gru_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/im2col.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/im2col.c.o"
  "/home/albert/project/31392_project/darknet/src/image.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/image.c.o"
  "/home/albert/project/31392_project/darknet/src/layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/layer.c.o"
  "/home/albert/project/31392_project/darknet/src/list.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/list.c.o"
  "/home/albert/project/31392_project/darknet/src/local_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/local_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/lstm_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/lstm_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/matrix.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/matrix.c.o"
  "/home/albert/project/31392_project/darknet/src/maxpool_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/maxpool_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/network.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/network.c.o"
  "/home/albert/project/31392_project/darknet/src/nightmare.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/nightmare.c.o"
  "/home/albert/project/31392_project/darknet/src/normalization_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/normalization_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/option_list.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/option_list.c.o"
  "/home/albert/project/31392_project/darknet/src/parser.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/parser.c.o"
  "/home/albert/project/31392_project/darknet/src/region_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/region_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/reorg_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/reorg_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/reorg_old_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/reorg_old_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/rnn.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/rnn.c.o"
  "/home/albert/project/31392_project/darknet/src/rnn_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/rnn_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/rnn_vid.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/rnn_vid.c.o"
  "/home/albert/project/31392_project/darknet/src/route_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/route_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/sam_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/sam_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/scale_channels_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/scale_channels_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/shortcut_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/shortcut_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/softmax_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/softmax_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/super.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/super.c.o"
  "/home/albert/project/31392_project/darknet/src/swag.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/swag.c.o"
  "/home/albert/project/31392_project/darknet/src/tag.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/tag.c.o"
  "/home/albert/project/31392_project/darknet/src/tree.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/tree.c.o"
  "/home/albert/project/31392_project/darknet/src/upsample_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/upsample_layer.c.o"
  "/home/albert/project/31392_project/darknet/src/utils.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/utils.c.o"
  "/home/albert/project/31392_project/darknet/src/voxel.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/voxel.c.o"
  "/home/albert/project/31392_project/darknet/src/writing.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/writing.c.o"
  "/home/albert/project/31392_project/darknet/src/yolo.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/yolo.c.o"
  "/home/albert/project/31392_project/darknet/src/yolo_layer.c" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/yolo_layer.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CUDNN"
  "GPU"
  "OPENCV"
  "USE_CMAKE_LIBS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  "../3rdparty/stb/include"
  "/usr/local/cuda-10.2/targets/x86_64-linux/include"
  "/usr/local/include/opencv4"
  )
set(CMAKE_DEPENDS_CHECK_CUDA
  "/home/albert/project/31392_project/darknet/src/activation_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/activation_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/avgpool_layer_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/avgpool_layer_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/blas_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/blas_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/col2im_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/col2im_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/convolutional_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/convolutional_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/crop_layer_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/crop_layer_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/deconvolutional_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/deconvolutional_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/dropout_layer_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/dropout_layer_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/im2col_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/im2col_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/maxpool_layer_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/maxpool_layer_kernels.cu.o"
  "/home/albert/project/31392_project/darknet/src/network_kernels.cu" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/network_kernels.cu.o"
  )
set(CMAKE_CUDA_COMPILER_ID "NVIDIA")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CUDA
  "CUDNN"
  "GPU"
  "OPENCV"
  "USE_CMAKE_LIBS"
  )

# The include file search paths:
set(CMAKE_CUDA_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  "../3rdparty/stb/include"
  "/usr/local/cuda-10.2/targets/x86_64-linux/include"
  "/usr/local/include/opencv4"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/albert/project/31392_project/darknet/src/http_stream.cpp" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/http_stream.cpp.o"
  "/home/albert/project/31392_project/darknet/src/image_opencv.cpp" "/home/albert/project/31392_project/darknet/build-release/CMakeFiles/darknet.dir/src/image_opencv.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CUDNN"
  "GPU"
  "OPENCV"
  "USE_CMAKE_LIBS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  "../3rdparty/stb/include"
  "/usr/local/cuda-10.2/targets/x86_64-linux/include"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
