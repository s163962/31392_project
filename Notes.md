# Notes


## Detector and Classifier
* YOLOv3 implementering (Darknet)
	* https://github.com/AlexeyAB/darknet

* YOLO weights baseret på Open Images
	* https://github.com/radekosmulski/yolo_open_images
	* `./darknet detector test ../detector/cfg/yolo.data ../detector/cfg/yolov3-spp.cfg ../detector/weights/yolov3-spp_final.weights -ext_output -thresh 0.025` fra `31391_project/darknet`


* Brug YOLO detector direkte fra Python
	* https://github.com/AlexeyAB/darknet/issues/5237
	* https://github.com/AlexeyAB/darknet/blob/master/darknet.py
	* `import darknet`
	* `python3 darknet_test`
	* Kan være at vi skal træne modellen yderligere med vores eget data, Open Images klarer sig okay på pap-kassen men ikke fantastisk.
	* Skal sikkert re-build'es på hver maskine
	* `pip install -e {directory}`
	* Resume training:
	`./darknet detector train ../31392_project/detector/cfg/yolo.data ../31392_project/detector/cfg/yolov3-spp.cfg ../31392_project/detector/weights/yolov3-spp_final.weights -map`


* Annotation værktøj til YOLO format
	* https://github.com/AlexeyAB/Yolo_mark



### Run all darknet commands from darknet dir
* ./darknet/darknet detector train detector/cfg/yolo31392.data yolov3-spp.cfg detector/weights/backup/yolov3-spp_31392_final.weigths -map

* ./darknet/darknet detector test ../31392_project/detector/cfg/yolo31392.data ../31392_project/detector/cfg/yolov3-spp.cfg ../31392_project/detector/weights/backup/yolov3-spp_31392_final.weigths -ext_output -thresh 0.10


* ./darknet detector test ../detector/cfg/yolo31392.data ../detector/cfg/yolov3-spp_31392.cfg ../detector/weights/backup/yolov3-spp_31392_final.weights -ext_output -thresh 0.10 ../data/train/box/box00000/00000.jpg

* ./darknet detector train ../detector/cfg/yolo31392-train.data ../detector/cfg/yolov3-spp_31392-train.cfg ../detector/weights/backup/yolov3-spp_31392_final.weights -map

* ./darknet detector map ../detector/cfg/yolo31392-train.data ../detector/cfg/yolov3-spp_31392-train.cfg ../detector/weights/backup/yolov3-spp_31392_final.weights

### Training progress
Transfer learning from https://github.com/radekosmulski/yolo_open_images with our own dataset.
Training started at iteration 500205 iteration

mAP: [weights (name), overall (mAP), cup (AP), book (AP), box (AP)]
2508 images, unique_truth_count = 2498 

../detector/weights/backup/yolov3-spp_31392_final.weights, 0.27%, 67.44%, 9.14%, 59.14%
 for conf_thresh = 0.25, precision = 0.36, recall = 0.35, F1-score = 0.36 
 for conf_thresh = 0.25, TP = 870, FP = 1527, FN = 1628, average IoU = 22.92 % 

../detector/weights/backup/yolov3-spp_31392-train_501000.weights, 0.41%, 98.16%, 94.26%, 12.14%
 for conf_thresh = 0.25, precision = 0.91, recall = 0.92, F1-score = 0.91 
 for conf_thresh = 0.25, TP = 2291, FP = 239, FN = 207, average IoU = 70.25 % 

../detector/weights/backup/yolov3-spp_31392-train_best.weights, 0.40%, 99.71%, 100%, 0.72%
 for conf_thresh = 0.25, precision = 0.99, recall = 0.94, F1-score = 0.97 
 for conf_thresh = 0.25, TP = 2358, FP = 28, FN = 140, average IoU = 77.52 % 

../detector/weights/backup/yolov3-spp_31392-train_last.weights, 0.40%, 99.69%, 96.81%, 5.78%
 for conf_thresh = 0.25, precision = 0.91, recall = 0.94, F1-score = 0.93 
 for conf_thresh = 0.25, TP = 2355, FP = 235, FN = 143, average IoU = 71.95 % 




