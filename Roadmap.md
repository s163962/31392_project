# TODO:
1. A Træn et netværk til at genkende bøger, kopper og kasser
1.1 Saml data
1.2 Lav en classification funktion der virker nice
1.3 Gem CNN som "navn.whatever" <- Ændrer den her efter hvad vi finder ud af

1. B Yolo på billederne
1.1 Få yolo til at køre i python
1.2 Få yolo til at genkende pakker/bøger
1.3 Test på vores dataset

2. Lav et stereo -> xyz 
2.1 Rectification af billeder (seperat funktion) --- Brug fisheye objektet i opencv
2.2 Classify skal give et output vi kan bruge til stereo(?)
2.3 xyz fra stereobilleder

3. Lav et 3D Kalman filter objekt
3.1 Predict og update, ourKalman.p() for predict, outKalman.u(measurement) for updating with measurements.

4. Lav et main.py dokument der kører det helper
4.1 Load CNN
4.2 

5. Lav helpers.py
5.1 getK(), som returnerer K fra det vi har fået udleveret.

# Data flow:
Hvordan vi har tænkt os at filerne skal arbejde sammen, og hvilken data der skal sendes mellem dem (og dataens formatering)
Run once:
"trainCNN.py"
Billeder -cv2-> trainCNN.py -sklearn-> "navn.whatever" 

Run with video's:
"main.py"
video.mp3 --cv2--> main.py --(leftbgr, rightbgr)--> rectify.py --(leftbgr, rightbgr)--> 
classifyAndLocation.py --(x,y,z,id)--> kalman.py --stateMatrix--> main.py ----> skærm


# Filer:
## helper.py:
Indeholder funktioner til nemt at importere billeder og data.
- getK() - Returnerer (K_left, K_right) som er steroekamera matricen

# Ting der skal skrives (10 sider):
- Hvordan fungerer Kalman
- Hvordan finder man steroematricer + 3D punkter fra 2 billeder
- Wide angle vs non-wide angle fisheye


# Vores objekter:
KalmanObj:
    Vars:
state [6x6 Matrix] could be extended to [9x9] if it works nicely
timeFound [frame#]
lastSeen [frame#]
    Functions:
p() -> Predicts the next state, should be used every frame
u(measurement) -> Accepts a measurement matrix [[x, y, z]], returns state matrix
getState() -> Returns the state matrix [6x6]
getTFound() -> Returns timeFound [frame#]
getTSeen() -> Returns lastSeen [frame#]

# Deadlines:

20. April - Arbejde efter forelæsning
11. Maj - Aflever
13. Maj - Svar m. om vi kan komme til eksamen eller ej


