import numpy as np
import cv2
import pickle
from rectify import rectify


def get_img(img, maps, scale=0.75):
    rect = cv2.remap(img, maps[0], maps[1], cv2.INTER_CUBIC)
    out = cv2.resize(rect, None, fx=scale, fy=scale)
    return out


def calc_world(pix, R, P):
    x_c = np.concatenate((pix, np.array([1])))


with open("pickle/calib_mono.pkl", "rb") as f:
    mtx, dist, rvecs, tvecs = pickle.load(f)

file_path = "../data/course/stereo_calibration_images"
frame = [[], []]
frame[0] = cv2.imread(file_path + "/left_0000.png")
frame[1] = cv2.imread(file_path + "/right_0000.png")

rect = [[], []]
maps, _, R, P, _ = rectify(frame[0].shape[:2], alpha=0, extra_out=True)
rect[0] = get_img(frame[0], maps[0])
rect[1] = get_img(frame[1], maps[1])

corners = [[], []]
pattern_size = (9, 6)
for i in range(2):
    ret, corners[i] = cv2.findChessboardCorners(rect[i], pattern_size)
    cv2.drawChessboardCorners(rect[i], pattern_size, corners[i], ret)

print(f"Corner 1:")
print(f"\t{corners[0][0,0]}\t{corners[1][0,0]}")


stack = np.hstack((rect[0], rect[1]))
cv2.imshow("both", stack)
cv2.waitKey(0)
cv2.destroyAllWindows()
