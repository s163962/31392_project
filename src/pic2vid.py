import cv2

show_frames = False

print("===============📷➡📽===============")

print("Loading video")
cap = cv2.VideoCapture("../video_without/%d.jpg")

ret, frame = cap.read()
if not ret:
    print("Error loading video")
    exit()
if ret:
    print("Creating output object")
    out = cv2.VideoWriter(
        "../fig/OutputWithoutOcclusions.mp4",
        cv2.VideoWriter_fourcc("m", "p", "4", "v"),
        24,
        (frame.shape[1], frame.shape[0]),
        isColor=True,
    )

print("Begins processing")
while ret:
    out.write(frame)
    if show_frames:
        cv2.imshow("Output video", frame)
        if cv2.waitKey(1) == 27:
            break
    ret, frame = cap.read()

print("Saving video")
out.release()
print("🎆🎇Done🎂✅")
print("===================================")
