import numpy as np
import cv2 as cv2
import scipy


class kalman:
    def __init__(self, A, C, sig2_x0=1e3, sig2_Q=0, sig2_R=1):
        nx = A.shape[0]
        nz = C.shape[0]

        self.nx = nx
        self.nz = nz
        self.K = None

        self.x = np.zeros((nx,))

        self.P = sig2_x0 * np.eye(nx)
        self.Q = np.eye(nx) * sig2_Q
        self.R = np.eye(nz) * sig2_R

        self.A = A
        self.C = C

    def predict(self):
        self.x = self.A @ self.x
        self.P = self.A @ self.P @ self.A.T + self.Q

    def update(self, measurement):
        S = self.C @ self.P @ self.C.T + self.R
        #        print("S")
        #        print(S)
        self.K = self.P @ self.C.T @ np.linalg.pinv(S)
        #        print("K")
        #        print(K)
        #        print("P")
        #        print(self.P)
        self.x = self.x + self.K @ (measurement - self.C @ self.x)
        self.P = (np.eye(self.nx) - self.K @ self.C) @ self.P
        # self.P = scipy.linalg.sqrtm(self.P.T @ self.P)

    def reset(self, sigma2=1e3, x=None):
        self.x = np.ones_like(self.x)
        if x:
            self.x = np.array(x)
        self.P = sigma2 * np.eye(self.nx)

    def draw(self, img, box, radius=4, color=(0, 0, 0)):
        coords = (self.C @ self.x).astype(np.int)
        cv2.circle(img, (coords[0], coords[1]), radius, color, thickness=-1)

        # cv2.rectangle(
        #     img, (box[0, 0], box[0, 1]), (box[2, 0], box[2, 1]), color, thickness=1
        # )

    def write(self, img):
        text_string = [
            f"Position:        x={x[0]:.2f}    y={x[3]:.2f}",
            f"Velocity:        x={x[1]:.2f}    y={x[4]:.2f}",
            f"Acceleration:    x={x[2]:.2f}    y={x[5]:.2f}",
        ]

    #     for i in range(len(text_string)):
    #         cv2.putText(
    #             img,
    #             text_string[i],
    #             org=(50, 100 + 30 * i),
    #             fontFace=cv2.FONT_HERSHEY_SIMPLEX,
    #             fontScale=1,
    #             color=(0, 0, 0),
    #         )
