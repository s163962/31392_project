import cv2 as cv2
from rectify import rectify
import numpy as np

"""
Plan 24-04-202020:
Program to track the center of a moving object
Functions using optical flow, on rectified images.
Good features to track is initially used to find features, 
and the openCV funtion for optical features is then used to determine their flow.
The object is isolated using outlier detection, and then finding a bounding box.
"""

DEBUG = True


def optical_tracking(vidCapObj):
    pass


def remove_outliers(features, max_distance=200):
    # Outlier detection, they have to be within limit of mean
    # TODO: Do an actual outlier detection, other than euclidian distance from mean
    features = np.squeeze(features)
    feat_mean_x = np.mean(features[:, 0])
    feat_mean_y = np.mean(features[:, 1])
    x_dist = features[:, 0] - feat_mean_x
    y_dist = features[:, 1] - feat_mean_y
    features = features[
        (np.abs(x_dist) < max_distance) & (np.abs(y_dist) < max_distance)
    ]
    return features


def find_bounding_box(features):
    # Find bounding box, order: upper left, upper right, lower right, lower left
    xmin = np.min(features[:, 0])
    xmax = np.max(features[:, 0])
    ymin = np.min(features[:, 1])
    ymax = np.max(features[:, 1])
    box = np.matrix([[xmin, ymin], [xmax, ymin], [xmax, ymax], [xmin, ymax]])
    return box


def draw_bounding_box(image, box, box_color=(0, 255, 0)):
    for i in range(box.shape[0] - 1):
        cv2.line(
            image, (box[i, 0], box[i, 1]), (box[i + 1, 0], box[i + 1, 1]), box_color, 2,
        )
    cv2.line(
        image, (box[3, 0], box[3, 1]), (box[0, 0], box[0, 1]), box_color, 2,
    )
    return image


def track_optical_flow(
    cap_left, cap_right, get_new_features=2, min_movement_dist=1, save_video=False
):
    # Read the first frame
    frame = [[], []]
    ret, frame[0] = cap_left.read()
    if not ret:
        print("Left file not found")
        exit()
    ret, frame[1] = cap_right.read()
    if not ret:
        print("Right file not found")
        exit()
    # Start working on it
    frame[0] = cv2.cvtColor(frame[0], cv2.COLOR_BGR2GRAY)
    frame[1] = cv2.cvtColor(frame[1], cv2.COLOR_BGR2GRAY)
    # Rectify
    maps, roi = rectify(frame[0].shape[:2], alpha=0, fisheye=0)
    rect = [[], []]
    rect[0] = cv2.remap(frame[0], maps[0][0], maps[0][1], cv2.INTER_CUBIC)
    rect[1] = cv2.remap(frame[1], maps[1][0], maps[1][1], cv2.INTER_CUBIC)
    # Find initial features
    prev_feat = feat = cv2.goodFeaturesToTrack(
        rect[0], maxCorners=600, qualityLevel=0.015, minDistance=6
    )
    prev_feat = feat = np.squeeze(feat)
    # Ready saving
    if save_video:
        out = cv2.VideoWriter(
            "left_tracker.mp4",
            cv2.VideoWriter_fourcc("m", "p", "4", "v"),
            24,
            (rect[0].shape[1], rect[0].shape[0]),
            isColor=True,
        )
    # Loop the video
    new_rect = [[], []]
    new_rect_gray = [[], []]
    loopcount = 0
    get_new_features = 2
    while True:
        # Load next cap, gray and rectify it
        ret, new_rect[0] = cap_left.read()
        if not ret:
            break
        new_rect[0] = cv2.remap(new_rect[0], maps[0][0], maps[0][1], cv2.INTER_CUBIC)
        new_rect_gray[0] = cv2.cvtColor(new_rect[0], cv2.COLOR_BGR2GRAY)
        # rect is the last gray image, new rect gray is the new gray image
        # If x get new features, append them to moving features
        if (loopcount % get_new_features) == 0:
            new_feat = cv2.goodFeaturesToTrack(
                rect[0], maxCorners=300, qualityLevel=0.015, minDistance=7
            )
            new_feat = np.squeeze(new_feat)
            temp = np.concatenate((prev_feat, new_feat), axis=0)
            prev_feat = temp
        feat, status, error = cv2.calcOpticalFlowPyrLK(
            rect[0], new_rect_gray[0], prev_feat, None
        )

        # If the features are far enough save them and draw lines between them
        moving_feat = []  # List of features that moved enough
        for i in range(len(prev_feat)):
            if cv2.norm(prev_feat[i] - feat[i]) > min_movement_dist:
                moving_feat.append(feat[i])
                cv2.line(
                    new_rect[0],
                    (prev_feat[i, 0], prev_feat[i, 1]),
                    (feat[i, 0], feat[i, 1]),
                    (0, 255, 0),
                    2,
                )
                cv2.circle(
                    new_rect[0], (feat[i, 0], feat[i, 1]), 5, (0, 255, 0), -1,
                )

        moving_feat = np.squeeze(moving_feat)
        moving_feat = remove_outliers(moving_feat, 150)
        # Find bounding box, order: upper left, upper right, lower right, lower left
        box = find_bounding_box(moving_feat)
        # Draw bounding box
        new_rect[0] = draw_bounding_box(new_rect[0], box)
        # Update variables
        rect[0] = new_rect_gray[0]
        prev_feat = moving_feat
        loopcount = loopcount + 1
        # Show the frame
        cv2.imshow("frame", new_rect[0])
        if save_video:
            out.write(new_rect[0])
        if cv2.waitKey(10) == 27:
            break
        # end of while loop
    cap_left.release()
    cap_right.release()
    if save_video:
        out.release()
    cv2.destroyAllWindows()
    return


if __name__ == "__main__":
    # Load the video
    cap_left = cv2.VideoCapture(
        "../data/course/sample_stereo_conveyor_without_occlusions/left/left_%04d.png"
    )
    cap_right = cv2.VideoCapture(
        "../data/course/sample_stereo_conveyor_without_occlusions/right/right_%04d.png"
    )
    track_optical_flow(cap_left, cap_right, save_video=False)
