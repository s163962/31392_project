import os

file_path = "../data/course/stereo_calibration_images/"
fnames = os.listdir(file_path)

for fname in fnames:
    name = file_path + fname
    os.rename(name, name.replace("-", "_"))
    # if fname.startswith("left"):
    #     os.rename(fname, "left_" + fname[-8:])
    # elif fname.startswith("right"):
    #     os.rename(fname, "right_" + fname[-8:])
