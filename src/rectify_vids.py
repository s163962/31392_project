import cv2
from rectify import rectify

if __name__ == "__main__":
    # Load vids
    
    input_path_left = "../data/course/Stereo_conveyor_with_occlusions/left/left_%04d.png"
    input_path_right = "../data/course/Stereo_conveyor_with_occlusions/right/right_%04d.png"
    
    print("Loading videos")
    cap_left = cv2.VideoCapture(input_path_left)
    print("Loaded " + input_path_left)
    cap_right = cv2.VideoCapture(input_path_right)
    print("Loaded " + input_path_right)

    frame = [[], []]
    ret, frame[0] = cap_left.read()
    ret, frame[1] = cap_right.read()
    if not ret:
        print("Left file not found")
        exit()

    print("Creating rectify object")
    maps, _, _ = rectify(frame[0].shape[:2], alpha=0)
    rect = [[], []]

    print("Creating save object")
#    filename_left = "../data/course/rectified/Stereo_conveyor_without_occlusions/left/left_{:04d}.png"
#    filename_right = "../data/course/rectified/Stereo_conveyor_without_occlusions/right/right_{:04d}.png"
    filename_left = "../data/course/rectified/Stereo_conveyor_with_occlusions/left/left_{:04d}.png"
    filename_right = "../data/course/rectified/Stereo_conveyor_with_occlusions/right/right_{:04d}.png"
    frame_num = 0
    
    while ret:
        print("Processing frame " + str(frame_num))

        for i in range(len(rect)):
            rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)
        ret = cv2.imwrite(filename_left.format(frame_num), rect[0])
        if not ret:
            print("Failed saving, try creating the folders")
            break
        ret = cv2.imwrite(filename_right.format(frame_num), rect[1])
        if not ret:
            print("Failed saving, try creating the folders")
            break

        ret, frame[0] = cap_left.read()
        ret, frame[1] = cap_right.read()
        frame_num = frame_num + 1

    print("Done")
