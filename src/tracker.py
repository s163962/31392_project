#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 19:06:17 2020

@author: albert
"""

import cv2
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from helpers import *
from kalman import *
from darknet import darknet


# Define types of objects
obj_class_list = ["cup", "book", "box"]
obj_class_count = len(obj_class_list)
obj_class_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
print(obj_class_color[0])

# Define detector
#   Input: any necessary data lol who knows
#   Output: detected, type of object detected, x_frame, y_frame, (height?), (width?)
#       x_frame and y_frame should be centroid or top left corner?
def detectObject(obj_class_list, data):
    # Init
    x_frame = 0
    y_frame = 0
    height = 0
    width = 0
    detected = False # Was an object detected?
    
    
    # Hmm
    # CNN
    detectorFilePath = "data/course/sample_stereo_conveyor_without_occlusions/left/left_0100.png"
    print("Performing detection of " + detectorFilePath)
    res = darknet.performDetect(imagePath     =detectorFilePath, 
                          thresh        = detectorThresh, 
                          configPath    = detectorConfigPath, 
                          weightPath    = detectorWeightPath,
                          metaPath      = detectorMetaPath,
                          showImage     = False,
                          makeImageOnly = False,
                          initOnly      = False)
    
    
    
    
    if detected:
        obj_class = obj_class_list[0]
        x_frame = 0
        y_frame = 0
        height = 0
        width = 0
    else:
        obj_class = "None"
        x_frame = 0
        y_frame = 0
        height = 0
        width = 0
    
    return detected, obj_class, x_frame, y_frame, height, width


# Define function for drawing object bounding box on frame
def drawObject(frame, obj_class_color, obj_type, x_frame, y_frame, height, width, prediction=False):
    # Currently assuming x_frame and y_frame is top left corner.
    
    # Get color
    color = obj_class_color[obj_class_list.index(obj_type)]
    text = obj_type
    if prediction:
        color = (255, 255, 255)
        text = text + " predicted"
    
    thickness_obj = 4
    thickness_text = 2
    
    # Define points
    frame_obj = np.copy(frame)
    x0 = x_frame
    y0 = y_frame
    x1 = x_frame + width
    y1 = y_frame + height
    
    # Draw
    cv2.rectangle(frame_obj, (x0, y0), (x1, y1), color, thickness_obj)
    cv2.putText(frame_obj, text, (x0, y0 - 5), cv2.FONT_HERSHEY_PLAIN, 1, color, thickness_text)
    
    return frame_obj


# ../31392_project/
# Lets use "yolov3-spp_31392_final.weights" as final weights name.
detectorConfigPath = "detector/cfg/yolov3-spp.cfg"
detectorWeightPath = "detector/weights/yolov3-spp_final.weights"
detectorMetaPath = "detector/cfg/yolo.data"


detectorThresh = 0.05 # Open Images weights without our own training occasionally has confidence around 0.03 on some sample boxes...


detectorFilePath = "data/course/sample_stereo_conveyor_without_occlusions/left/left_0042.png"
print("Initialising network")
darknet.performDetect(imagePath     =detectorFilePath, 
                      thresh        = detectorThresh, 
                      configPath    = detectorConfigPath, 
                      weightPath    = detectorWeightPath,
                      metaPath      = detectorMetaPath,
                      showImage     = False,
                      makeImageOnly = False,
                      initOnly      = True)



## Init
# Initialise Kalman for each object
# ...
## Loop
# Detect
# Update Kalman
# Visualise
    
obj_type = "box"
img = np.zeros((500, 500, 3))
img_obj = drawObject(img, obj_class_color, obj_type, 100, 100, 50, 75)
frame = drawObject(img_obj, obj_class_color, obj_type, 150, 200, 50, 75, prediction=True)
plt.imshow(frame)