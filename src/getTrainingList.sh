#!/bin/bash

# You better run this file from the prefix dir

prefix="data/"
file_paths=`find train -type f -name "*.jpg"`

for file in $file_paths
do
    echo $prefix$file >> yolo31392.train
done

file_paths=`find train -type f -name "*.png"`

for file in $file_paths
do
    echo $prefix$file >> yolo31392.train
done

file_paths=`find valid -type f -name "*.jpg"`

for file in $file_paths
do
    echo $prefix$file >> yolo31392.valid
done

file_paths=`find valid -type f -name "*.png"`

for file in $file_paths
do
    echo $prefix$file >> yolo31392.valid
done
