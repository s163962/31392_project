import numpy as np
import cv2
import pickle

try:
    from src.rectify import rectify
except:
    from rectify import rectify


# Function to create point cloud file
def create_output(vertices, colors, filename):
    colors = colors.reshape(-1, 3)
    vertices = np.hstack([vertices.reshape(-1, 3), colors])

    ply_header = """ply
		format ascii 1.0
		element vertex %(vert_num)d
		property float x
		property float y
		property float z
		property uchar red
		property uchar green
		property uchar blue
		end_header
		"""
    with open(filename, "w") as f:
        f.write(ply_header % dict(vert_num=len(vertices)))
        np.savetxt(f, vertices, "%f %f %f %d %d %d")


def downsample_image(image, reduce_factor):
    for i in range(0, reduce_factor):
        # Check if image is color or grayscale
        if len(image.shape) > 2:
            row, col = image.shape[:2]
        else:
            row, col = image.shape

        image = cv2.pyrDown(image, dstsize=(col // 2, row // 2))
    return image


class ourDisparity:
    def __init__(self,):
        print("Creating stereo object")
        self.stereo = cv2.StereoSGBM_create()
        self.stereo.setNumDisparities(16 * 14)  # Must be mult of 16,found to be ~160
        # self.stereo.setBlockSize(25)

    def __init__(self, bm_disps=None, bm_bs=None):
        print("Creating bm stereo object, for tuning")
        if bm_disps is None:
            self.stereo = cv2.StereoSGBM_create(
                numDisparities=16 * 20,
                blockSize=17,
                P1=0,
                P2=0,
                disp12MaxDiff=0,
                uniquenessRatio=0,
                speckleWindowSize=0,
                speckleRange=0,
            )
            # self.stereo.setNumDisparities(
            #    16 * 14
            # )  # Must be mult of 16,found to be ~160

            # self.stereo.setBlockSize(25)
        else:
            self.stereo = cv2.StereoBM_create(bm_disps, bm_bs)

    def draw_bounding_box(self, image, box, box_color=(0, 255, 0)):
        for i in range(box.shape[0] - 1):
            cv2.line(
                image,
                (int(box[i, 0]), int(box[i, 1])),
                (int(box[i + 1, 0]), int(box[i + 1, 1])),
                box_color,
                2,
            )
        cv2.line(
            image,
            (int(box[3, 0]), int(box[3, 1])),
            (int(box[0, 0]), int(box[0, 1])),
            box_color,
            2,
        )
        return image

    def getDisparityRaw(self, rect_color_left, rect_color_right):
        return self.stereo.compute(rect_color_left, rect_color_right)

    def getDisparity32bit(self, rect_color_left, rect_color_right):
        temp = self.stereo.compute(rect_color_left, rect_color_right)
        temp = temp / 16
        return temp.astype(np.float32)

    def getDisparity0to1(self, rect_color_left, rect_color_right):
        temp = self.stereo.compute(rect_color_left, rect_color_right)
        return temp / np.max(temp)

    def getDisparity(self, rect_color_left, rect_color_right):
        # Always update to our state of the art whatever
        return self.getDisparity0to1(rect_color_left, rect_color_right)

    def getDistanceToObject(self, rect_color_left, rect_color_right, box):
        # Returns a distance, but, unknown unit
        # Input checking:
        if box[0] is None:
            box = box[2]
        if box[0, 0] < 1:
            raise TypeError("You need a proper box as input")
        disparityMap = self.getDisparityRaw(rect_color_left, rect_color_right)
        disparityMap = disparityMap / 16
        xmin = int(np.min(box[:, 0]))
        ymin = int(np.min(box[:, 1]))
        xmax = int(np.max(box[:, 0]))
        ymax = int(np.max(box[:, 1]))
        cutout = disparityMap[ymin:ymax, xmin:xmax]
#        print("Cutout max: {}\tmin: {}".format(np.max(cutout), np.min(cutout)))
        mask = cutout > 0
        area_pixels = cutout[mask].shape[0]
        average_intensity = np.sum(cutout[mask]) / area_pixels
        return average_intensity


def attempt_at_3d(rect, disparity_obj):
    disparity = disp.getDisparityRaw(rect[0], rect[1])
    h, w = rect[0].shape[:2]
    # disparity = disparity.astype(np.float32)
    print("Loading stereo parameters...")
    with open("pickle/calib_stereo.pkl", "rb") as f:
        mtx, dist, R, T, _, _ = pickle.load(f)
    focal_length = mtx[0][0, 0]
    Q1 = np.float32(
        [
            [1, 0, 0, -w / 2.0],
            [0, -1, 0, h / 2.0],
            [0, 0, 0, -focal_length],
            [0, 0, 1, 0],
        ]
    )
    Q2 = np.float32(
        [[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, focal_length * 0.05, 0,], [0, 0, 0, 1],]
    )
    disparity_scaled = disparity / 16
    points_3D = cv2.reprojectImageTo3D(
        disparity_scaled.astype(np.float32), Q2
    )  # disp/16 from documentation
    colors = cv2.cvtColor(rect[0], cv2.COLOR_BGR2RGB)
    mask_map = disparity > disparity.min()
    output_points = points_3D
    output_colors = colors
    output_file = "reconstructed.ply"
    # Generate point cloud

    print("\n Creating the output file... \n")
    create_output(output_points, output_colors, output_file)
    print("Reprojected")
    cv2.waitKey()
    cv2.waitKey()


if __name__ == "__main__":
    cap_left = cv2.VideoCapture(
        "data/course/sample_stereo_conveyor_without_occlusions/left/left_%04d.png"
    )
    cap_right = cv2.VideoCapture(
        "data/course/sample_stereo_conveyor_without_occlusions/right/right_%04d.png"
    )
    print("Create disparity object")
    disp = ourDisparity()
    disp_bm = ourDisparity(240, 25)

    frame = [[], []]
    ret, frame[0] = cap_right.read()
    ret, frame[1] = cap_right.read()
    if not ret:
        print("no im")
        exit()
    maps, roi, Q = rectify(frame[0].shape[:2], alpha=0)
    rect = [[], []]
    gray = [[], []]

    for i in range(len(rect)):
        rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)

    # img_1_downsampled = downsample_image(rect[0], 3)
    # img_2_downsampled = downsample_image(rect[1], 3)
    disparity = disp.getDisparityRaw(rect[0], rect[1])
    h, w = rect[0].shape[:2]
    # disparity = disparity.astype(np.float32)
    print("Loading stereo parameters...")
    with open("pickle/calib_stereo.pkl", "rb") as f:
        mtx, dist, R, T, _, _ = pickle.load(f)
    focal_length = mtx[0][0, 0]
    Q1 = np.float32(
        [
            [1, 0, 0, -w / 2.0],
            [0, -1, 0, h / 2.0],
            [0, 0, 0, -focal_length],
            [0, 0, 1, 0],
        ]
    )
    Q2 = np.float32(
        [[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, focal_length * 0.05, 0,], [0, 0, 0, 1],]
    )
    disp_scaled = disparity / 16.0
    disp_scaled = disp_scaled.astype(np.float32)
    mask = disp_scaled > 0
    points_3D = cv2.reprojectImageTo3D(disp_scaled, Q)
    points_3D1 = cv2.reprojectImageTo3D(disp_scaled, Q1)
    points_3D2 = cv2.reprojectImageTo3D(disp_scaled, Q2)
    colors = cv2.cvtColor(rect[0], cv2.COLOR_BGR2RGB)
    mask_map = disparity > disparity.min()
    output_points = points_3D[mask]
    output_colors = colors[mask]
    output_file = "reconstructed.ply"
    # Generate point cloud
    cv2.imshow("test2", disp_scaled)
    cv2.imshow("test1", disparity)
    cv2.waitKey()

    print("\n Creating the output file... \n")
    # create_output(output_points, output_colors, output_file)
    # create_output(points_3D1[mask], output_colors, "reconstructed1.ply")
    # create_output(points_3D2[mask], output_colors, "reconstructed2.ply")
    print("Reprojected")
    # cv2.waitKey()
    # cv2.waitKey()
    show = False
    ret = False  # Don't run loop

    while ret:
        for i in range(len(rect)):
            rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)
        for i in range(len(gray)):
            gray[i] = cv2.cvtColor(rect[i], cv2.COLOR_BGR2GRAY)

        if show:
            output = disp.getDisparityRaw(rect[0], rect[1])
            # print(np.min(output))
            output_norm = (output - np.min(output)) / np.max(output)
            # print(np.min(output))
            # print(np.max(output))
            # output = disp.getDisparity(gray[0], gray[1])

        box = np.matrix(
            [
                [881.2883, 304.68552],
                [1023.9358, 304.68552],
                [1023.9358, 414.70963],
                [881.2883, 414.70963],
            ]
        )
        print(disp.getDistanceToObject(rect[0], rect[1], box))
        if show:
            withbox = disp.draw_bounding_box(output, box)
            cv2.imshow("Disparity", output_norm)

        ret, frame[0] = cap_left.read()
        _, frame[1] = cap_right.read()
        keypress = cv2.waitKey(1)
        if keypress == 27:
            break
    cv2.destroyAllWindows()
