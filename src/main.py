import cv2
import numpy as np

try:
    from src.rectify import rectify
    from src.basic_tracking import ObjectTracker
    from src.kalman import kalman
    from src.our_disparity import ourDisparity
except:
    from rectify import rectify
    from basic_tracking import ObjectTracker
    from kalman import kalman
    from our_disparity import ourDisparity


def cap_rect(cap, map):
    ret, img = cap.read()
    if ret:
        rect = cv2.remap(img, map[0], map[1], cv2.INTER_CUBIC)
    else:
        rect = None
    return ret, rect


def get_rect(img, map):
    rect = cv2.remap(img, map[0], map[1], cv2.INTER_CUBIC)
    return rect


def draw_roi(img, roi):
    cv2.rectangle(
        img, (roi[0], roi[2]), (roi[1], roi[3]), color=(0, 255, 0), thickness=1
    )


print("Main File for Conveyor Tracking")
print("===============================")

file_path = "data/course/Stereo_conveyor_without_occlusions"
print(f"Loading images from {file_path}...")
cap_left = cv2.VideoCapture(file_path + "/left/left_%04d.png")
cap_right = cv2.VideoCapture(file_path + "/right/right_%04d.png")

frame = [[], []]
ret, frame[1] = cap_right.read()
ret, frame[0] = cap_left.read()
if not ret:
    print("Left file not found")
    exit()

alpha = 0
print(f"Loading rectify maps for alpha={alpha}...")
maps, _, _ = rectify(frame[0].shape[:2], alpha=alpha)
rect = get_rect(frame[0], maps[0])

print(f"Creating tracking object...")
# roi = [370, 1240, 300, 690]
roi = None

if "without" in file_path:
    box_video = False
else:
    box_video = True

tracker = ObjectTracker(rect, roi=roi, draw=True, save_video=False, box_video=box_video)
# ret, rect = cap_rect(cap_left, maps[0])

print(f"Creating KF object...")
A = np.array(
    [
        [1, 1, 0.5, 0, 0, 0],
        [0, 1, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0.5],
        [0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 1],
    ]
)
# A = np.array([[1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 1, 1], [0, 0, 0, 1]])
C = np.array([[1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0],])
# C = np.array([[1, 0, 0, 0], [0, 0, 1, 0], [1, 0, 0, 0], [0, 0, 1, 0]])

kf = kalman(A, C, sig2_x0=1e3, sig2_Q=1e-2, sig2_R=[5e-2, 5e-2])
found = False
disp = ourDisparity()

print(f"Begin tracking...")
mask = tracker.generate_mask(rect.copy())
static_img = np.zeros_like(rect, np.uint64)
loopcount = 0
mask_length = 80
while True:
    ret, rect = cap_rect(cap_right, maps[1])
    if not ret:
        break
    ret, rect_left = cap_rect(cap_left, maps[0])
    if loopcount < mask_length:
        # generates a better mask
        mask = cv2.bitwise_or(mask, tracker.generate_mask(rect.copy()))
        static_img = cv2.add(np.uint64(rect), np.uint64(static_img))
    if loopcount == mask_length:
        static_img = np.uint8(static_img / mask_length)

    if loopcount % 10 == 0:
        make_new_points = True
    else:
        make_new_points = False
    if loopcount >= mask_length:
        box, center, moving_features, picture = tracker.track(
            rect,
            get_new_features=make_new_points,
            outlier_dist=150,
            mask=mask,
            static_img=static_img,
            corner=False,  # TODO fix corners if True
        )
    else:
        loopcount = loopcount + 1
        continue
    # TODO Make and to or so it updates if there is 1 point
    if center[2]:
        found = True
        kf.update(np.array(center[2]).reshape(-1,))
        if not (box[2][0, 0] < 1):
            print(disp.getDistanceToObject(rect_left, rect, box))
    if found:
        kf.predict()
        # kf.draw(picture, box[0])
        # kf.draw(picture, box[1])
        kf.draw(picture, box[2])

    if tracker.obj_in_frames < 1:
        print(loopcount, "rest")
        # kf.reset(x=[1120, 0, 0, 342, 0, 0])
    # ret, rect = cap_rect(cap_left, maps[0])
    # if tracker.draw:
    #     # print("Number of features: {}".format(len(moving_features)))
    #     # print("Loopcount: {}".format(loopcount))
    #     cv2.imshow("left", picture)
    #     if cv2.waitKey(0) == 27:
    #         break

    if roi is not None:
        draw_roi(picture, roi)
    cv2.imshow("left", picture)
    if cv2.waitKey(1) == 27:
        break
    loopcount = loopcount + 1
print("Finished tracking, closing down...")
del tracker  # kinda important
cv2.destroyAllWindows()  # Safety
