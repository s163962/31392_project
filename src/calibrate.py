import numpy as np
import cv2 as cv2
import glob
import pickle


def find_cb_corners(images, pattern_size=(9, 6), wait_time=500, show_img=False):
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((pattern_size[0] * pattern_size[1], 1, 3), np.float32)
    objp[:, 0, :2] = np.indices(pattern_size).T.reshape(-1, 2)

    objpoints = []
    imgpoints = []

    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Implement findChessboardCorners here
        ret, corners = cv2.findChessboardCorners(
            gray,
            patternSize=pattern_size,
            flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE,
        )
        # If found, add object points, image points (after refining them)
        if ret is True:
            objpoints.append(objp)
            cv2.cornerSubPix(gray, corners, (15, 15), (-1, -1), criteria)
            imgpoints.append(corners)

            if show_img is True:
                # Draw and display the corners
                img = cv2.drawChessboardCorners(img, pattern_size, corners, ret)
                cv2.imshow("img", img)
                cv2.waitKey(wait_time)

    return objpoints, imgpoints


def calib_mono(objpoints, imgpoints, gray, fisheye=0, show=1):
    calib_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-5)

    if not fisheye:
        calib_flags = cv2.CALIB_RATIONAL_MODEL
        retval, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
            objpoints,
            imgpoints,
            gray.shape[::-1],
            None,
            None,
            flags=calib_flags,
            criteria=calib_criteria,
        )
    else:
        calib_flags = (
            cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC
            + cv2.fisheye.CALIB_CHECK_COND
            + cv2.fisheye.CALIB_FIX_SKEW
        )
        retval, mtx, dist, _, _ = cv2.fisheye.calibrate(
            objpoints,
            imgpoints,
            gray.shape[::-1],
            None,
            None,
            flags=calib_flags,
            criteria=calib_criteria,
        )

    if show == 1:
        print("\t rms error: ", retval)
    elif show > 1:
        print("\t rms error: ", retval)
        print(mtx)
        print(dist)

    return mtx, dist, rvecs, tvecs


def calib_stereo(objpoints, imgpoints, mtx, dist, gray, fisheye=0, show=1):
    mtx_new = [[], []]
    dist_new = [[], []]

    calib_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-5)
    if not fisheye:
        calib_flags = (
            cv2.CALIB_USE_INTRINSIC_GUESS
            # cv2.CALIB_FIX_INTRINSIC
            # + cv2.CALIB_FIX_ASPECT_RATIO
            # + cv2.CALIB_FIX_FOCAL_LENGTH
            # + cv2.CALIB_SAME_FOCAL_LENGTH
            + cv2.CALIB_RATIONAL_MODEL
            # + cv2.CALIB_ZERO_TANGENT_DIST
            # + cv2.CALIB_ZERO_DISPARITY
            # + cv2.CALIB_FIX_K3
            # + cv2.CALIB_FIX_K4
            # + cv2.CALIB_FIX_K5
            # + cv2.CALIB_FIX_PRINCIPAL_POINT
        )

        (
            retval,
            mtx_new[0],
            dist_new[0],
            mtx_new[1],
            dist_new[1],
            R,
            T,
            E,
            F,
        ) = cv2.stereoCalibrate(
            objpoints,
            imgpoints[0],
            imgpoints[1],
            mtx[0],
            dist[0],
            mtx[1],
            dist[1],
            gray.shape[::-1],
            flags=calib_flags,
            criteria=calib_criteria,
        )
    else:
        # https://stackoverflow.com/questions/50603978/fisheye-lens-stereo-calibration-opencv-python
        # https://github.com/opencv/opencv/issues/11085
        # https://github.com/opencv/opencv/issues/5534
        calib_flags = (
            cv2.fisheye.CALIB_USE_INTRINSIC_GUESS
            + cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC
            + cv2.fisheye.CALIB_CHECK_COND
            + cv2.fisheye.CALIB_FIX_SKEW
            + cv2.fisheye.CALIB_FIX_PRINCIPAL_POINT
        )
        (
            retval,
            mtx_new[0],
            dist_new[0],
            mtx_new[1],
            dist_new[1],
            R,
            T,
        ) = cv2.fisheye.stereoCalibrate(
            objpoints,
            imgpoints[0],
            imgpoints[1],
            mtx[0],
            dist[0],
            mtx[1],
            dist[1],
            gray.shape[::-1],
            flags=calib_flags,
            criteria=calib_criteria,
        )
        E = []
        F, _ = cv2.findFundamentalMat(
            imgpoints[0][0], imgpoints[1][0], method=cv2.FM_LMEDS
        )

    if show == 1:
        print("\t rms error: ", retval)
    elif show > 1:
        print("\t rms error: ", retval)
        for i in range(len(mtx_new)):
            if (mtx_new[i] == mtx[i]).all():
                print(f"\t mtx {i} matching")
            else:
                print(f"\t mtx {i} changed")
            if (dist_new[i][0:4] == dist[i][0:4]).all():
                print(f"\t dist {i} matching")
            else:
                print(f"\t dist {i} changed")

    return mtx_new, dist_new, R, T, E, F


def calibrate(
    file_path="data/course/stereo_calibration_images",
    fisheye=0,
    show_img=0,
    show_txt=1,
    save=1,
    compare=True,
):
    # file_path = "../data/course/stereo_calibration_images"
    # fisheye = 1
    print(f"Starting Camera Calibration with Fisheye:{fisheye}")
    print(f"==================================================")
    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = [[], []]  # 2d points in image plane. [left, right]

    # Lists to store calibration params
    mtx = [[], []]
    dist = [[], []]
    rvecs = [[], []]
    tvecs = [[], []]

    print("Left camera")
    images = sorted(glob.glob(file_path + "/left_*.png"))
    objpoints, imgpoints[0] = find_cb_corners(images)
    print("Calibrating...")
    frame_left = cv2.imread(images[0], cv2.IMREAD_GRAYSCALE)
    mtx[0], dist[0], rvecs[0], tvecs[0] = calib_mono(
        objpoints, imgpoints[0], frame_left, fisheye=fisheye, show=show_txt
    )

    print("Right camera")
    images = sorted(glob.glob(file_path + "/right_*.png"))
    objpoints, imgpoints[1] = find_cb_corners(images)
    print("Calibrating...")
    frame_right = cv2.imread(images[0], cv2.IMREAD_GRAYSCALE)
    mtx[1], dist[1], rvecs[1], tvecs[1] = calib_mono(
        objpoints, imgpoints[1], frame_right, fisheye=fisheye, show=show_txt
    )

    if show_img:
        undist_left = cv2.undistort(frame_left, mtx[0], dist[0])
        cv2.imshow("undistorted left", undist_left)
        undist_right = cv2.undistort(frame_right, mtx[1], dist[1])
        cv2.imshow("undistorted right", undist_right)

        if cv2.waitKey(0) == 27:
            cv2.destroyAllWindows()

    print("Stereo cameras")
    print("Calibrating...")
    mtx_new, dist_new, R, T, E, F = calib_stereo(
        objpoints, imgpoints, mtx, dist, frame_left, fisheye=fisheye, show=show_txt
    )
    if compare:
        with open("src/pickle/calib_stereo.pkl", "rb") as f:
            mtx_old, dist_old, R_old, T_old, E_old, F_old = pickle.load(f)
        with open("src/pickle/calib_mono.pkl", "rb") as f:
            mtx_mono_old, dist_mono_old, rvecs_old, tvecs_old = pickle.load(f)
        if (mtx_old[0] == mtx_new[0]).all():
            print("Mtx[0] is not updated")
        else:
            print("Mtx[0] difference:")
            print(mtx_old[0] - mtx_new[0])
        if (mtx_old[1] == mtx_new[1]).all():
            print("Mtx[1] is not updated")
        else:
            print("Mtx[1] difference:")
            print(mtx_old[1] - mtx_new[1])
        if (dist_old[0] == dist_new[0]).all():
            print("dist[0] is not updated")
        else:
            print("dist[0] difference:")
            print(dist_old[0] - dist_new[0])
        if (dist_old[1] == dist_new[1]).all():
            print("dist[1] is not updated")
        else:
            print("dist[1] difference:")
            print(dist_old[1] - dist_new[1])
        if (R_old == R).all():
            print("R is not updated")
        else:
            print("Difference in R:")
            print(R_old - R)
        if (T_old == T).all():
            print("T is not updated")
        else:
            print("Difference in T:")
            print(T_old - T)
        if (F_old == F).all():
            print("F is not updated")
        else:
            print("Difference in F:")
            print(F_old - F)
    if save:
        print("Saving calibration intrinsics...")
        with open("src/pickle/calib_mono.pkl", "wb") as f:
            pickle.dump([mtx, dist, rvecs, tvecs], f)
        print("Saving calibration extrinsics...")
        with open("src/pickle/calib_stereo.pkl", "wb") as f:
            pickle.dump([mtx_new, dist_new, R, T, E, F], f)
    print("Done")


if __name__ == "__main__":
    calibrate(fisheye=0, show_img=0, show_txt=1, save=1)
