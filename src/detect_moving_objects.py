import cv2 as cv2
from rectify import rectify
import numpy as np

"""
Plan 24-04-202020:
Program to track the center of a moving object
Functions using optical flow, on rectified images.
Good features to track is initially used to find features, 
and the openCV funtion for optical features is then used to determine their flow.
The object is isolated using outlier detection, and then finding a bounding box.
Plan 27-04-202020:
Object for object tracking. Should be passed each new frame and x, returns y. tbd
Init with z and then call obj.whatever(frame_left, frame_right) to track each new frame (rect?)

I should say something about how to use this. Check the __main__ at the bottom for an example
"""


class ObjectTracker:
    def __init__(
        self, frame, show_frames=True, save_video=False, debug=False,
    ):
        """Should be initialized with pictures of the correct size, idieally the first frames of left/right
        frame_left: An image the size of the ones used in the video, in bgr"""

        # shared vars
        self.DEBUG_tracker = debug
        self.save_video = save_video
        self.prev_feat = None
        self.frames_processed_int = 0
        self.out_left = None
        self.show_frames = show_frames
        self.center = None
        self.obj_in_frames = 0

        # delete me later
        self.frame = None

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if self.DEBUG_tracker:
            cv2.imshow("frame", frame)
            cv2.waitKey(0)
            cv2.destroyWindow("frame")
        # Find initial features
        feat = cv2.goodFeaturesToTrack(
            gray, maxCorners=600, qualityLevel=0.015, minDistance=6
        )
        self.prev_feat = feat = np.squeeze(feat)  # Fuck numpy
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.prev_frame = gray
        # Ready saving, glhf
        if self.save_video:
            self.out_left = cv2.VideoWriter(
                "left_tracker.mp4",
                cv2.VideoWriter_fourcc("m", "p", "4", "v"),
                24,
                (frame.shape[1], frame[0].shape[0]),
                isColor=True,
            )

    def remove_outliers(self, features, max_distance=200):
        # Outlier detection, they have to be within limit of mean
        # TODO: Do an actual outlier detection, other than euclidian distance from mean
        features = np.squeeze(features)
        if len(features) < 4:
            return features
        feat_mean_x = np.mean(features[:, 0])
        feat_mean_y = np.mean(features[:, 1])
        x_dist = features[:, 0] - feat_mean_x
        y_dist = features[:, 1] - feat_mean_y
        features = features[
            (np.abs(x_dist) < max_distance) & (np.abs(y_dist) < max_distance)
        ]
        return features

    def find_bounding_box(self, features):
        # Find bounding box, order: upper left, upper right, lower right, lower left
        if len(features) < 4:
            box = np.matrix([[0, 0], [0, 0], [0, 0], [0, 0]])
            center = None
            return box, center

        xmin = np.min(features[:, 0])
        xmax = np.max(features[:, 0])
        ymin = np.min(features[:, 1])
        ymax = np.max(features[:, 1])
        box = np.matrix([[xmin, ymin], [xmax, ymin], [xmax, ymax], [xmin, ymax]])
        center = (int((xmin + xmax) / 2), int((ymin + ymax) / 2))
        return box, center

    def find_better_bounding_box(self, edgy_edges, box, center):
        pos = 50
        neg = -pos
        kernel = np.ones((5, 5), np.uint8)

        xmin = int(np.min(box[:, 0]) + neg)
        ymin = int(np.min(box[:, 1]) + neg)
        xmax = int(np.max(box[:, 0]) + pos)
        ymax = int(np.max(box[:, 1]) + pos)

        mask = np.zeros_like(edgy_edges)
        cv2.rectangle(mask, (xmin, ymin), (xmax, ymax), 255, -1)
        edgy_edges[mask == 0] = 0
        edgy_blobs = cv2.dilate(edgy_edges, kernel, iterations=10)
        edgy_blobs = cv2.erode(edgy_blobs, kernel, iterations=2)

        ret, thresh = cv2.threshold(edgy_blobs, 50, 255, cv2.THRESH_BINARY)
        im, contours, hierarchy = cv2.findContours(
            thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
        )

        cv2.imshow("test2", edgy_blobs)

        areas = [cv2.contourArea(cnt) for cnt in contours]
        if not areas:
            print("no valid contour")
            return box, center

        minArea = max(areas) / 1.1

        for i in range(len(areas)):
            area = areas[i]
            # print(self.frames_processed_int, area, minArea)
            M = cv2.moments(contours[i])
            if M["m00"] == 0:
                continue
            temp_center = [round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"])]
            if self.center is None:
                continue
            if (
                area > minArea
                and cv2.norm(np.asarray(temp_center) - np.asarray(list(self.center)))
                < 30
            ):
                color2 = (1, 255, 253)

                cv2.drawContours(self.frame, [contours[i]], 0, color2, 2)
                cv2.circle(
                    self.frame, tuple(temp_center), 5, (0, 255, 0), -1,
                )
                xmin = int(np.min(contours[i][:, 0, 0]))
                ymin = int(np.min(contours[i][:, 0, 1]))
                xmax = int(np.max(contours[i][:, 0, 0]))
                ymax = int(np.max(contours[i][:, 0, 1]))
                break

        cv2.imshow("test", self.frame)

        box = np.matrix([[xmin, ymin], [xmax, ymin], [xmax, ymax], [xmin, ymax]])
        temp_center = [int((xmax + xmin) / 2), int((ymax + ymin) / 2)]

        return box, tuple(temp_center)

    def draw_bounding_box(
        self, image, box, center, box_color=(0, 255, 0), center_color=(0, 0, 255)
    ):
        for i in range(box.shape[0] - 1):
            cv2.line(
                image,
                (box[i, 0], box[i, 1]),
                (box[i + 1, 0], box[i + 1, 1]),
                box_color,
                2,
            )
        cv2.line(
            image, (box[3, 0], box[3, 1]), (box[0, 0], box[0, 1]), box_color, 2,
        )
        cv2.circle(image, center, 5, center_color, thickness=-1)
        return image

    def release(self):
        print("Releasing associated objects")
        if self.save_video:
            self.out_left.release()
        cv2.destroyWindow("frame")

    def background_preprocess(self, frame, ksize=3, ksize2=15):
        # frame = cv2.medianBlur(frame, ksize2)
        frame = cv2.GaussianBlur(frame, (ksize, ksize), 1)
        return frame

    def generate_mask(self, static_img):
        kernel = np.ones((3, 3), np.uint8)
        static_img = self.background_preprocess(static_img)
        canny1 = cv2.Canny(static_img, 10, 150)
        mask = cv2.dilate(canny1, kernel, iterations=2)

        # cv2.imshow("frame3", mask)

        return mask

    # TODO new
    def apply_mask(self, important_edges, mask, mask2, size):
        M = cv2.moments(important_edges)
        if M["m00"] > 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])

            empty_img = np.zeros_like(mask)
            mask2 = cv2.circle(empty_img, (cX, cY), size, (255), thickness=-1)
            mask3 = 255 - cv2.bitwise_or(255 - mask2, mask)
            very_important_edges = cv2.bitwise_and(important_edges, mask3)

            return very_important_edges, 255 - mask3, mask2, (cX, cY)
        else:
            return important_edges, mask, mask2, None

    # TODO new
    def find_mask(self, frame, mask, scale=1):
        scale = (scale + 100) / 100
        # print(scale)
        frame = self.background_preprocess(frame)

        canny2 = cv2.Canny(frame, 30, 130)
        important_edges = cv2.bitwise_and((255 - mask), canny2)
        # cv2.imshow("frame4", important_edges)

        self.frame = frame

        return important_edges, mask

    def track(
        self,
        frame,
        get_new_features=False,
        min_movement_dist=1,
        outlier_dist=150,
        max_corners_gftt=300,
        mask=None,
    ):
        # TODO: Input checking, is it a picture?
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # If x get new features, append them to moving features
        if get_new_features:
            new_feat = cv2.goodFeaturesToTrack(
                gray, maxCorners=max_corners_gftt, qualityLevel=0.015, minDistance=7,
            )
            new_feat = np.squeeze(new_feat)
            if len(self.prev_feat) > 3:
                temp = np.concatenate((self.prev_feat, new_feat), axis=0)
                self.prev_feat = temp
            else:
                self.prev_feat = new_feat
        if len(self.prev_feat) < 4:
            keypress = None
            box, center = self.find_bounding_box(self.prev_feat)
            return keypress, box, center, self.prev_feat, frame
        # Get the optical flow
        feat, status, error = cv2.calcOpticalFlowPyrLK(
            self.prev_frame, gray, self.prev_feat, None
        )

        # If the features are far enough save them and draw lines between them
        moving_feat = []  # List of features that moved enough
        if self.show_frames or self.save_video:
            painted_frame = frame.copy()

        # TODO new
        edgy_edges, mask2 = self.find_mask(frame, mask, scale=self.obj_in_frames)
        for i in range(len(self.prev_feat)):

            if (
                cv2.norm(self.prev_feat[i] - feat[i]) > min_movement_dist
            ):  # insert mask check

                moving_feat.append(feat[i])
                if self.show_frames or self.save_video:  # Only draw if showing
                    cv2.line(
                        painted_frame,
                        (self.prev_feat[i, 0], self.prev_feat[i, 1]),
                        (feat[i, 0], feat[i, 1]),
                        (0, 255, 0),
                        2,
                    )
                    cv2.circle(
                        painted_frame, (feat[i, 0], feat[i, 1]), 5, (0, 255, 0), -1,
                    )
        # Try to isolate the object
        moving_feat = np.squeeze(moving_feat)
        if self.DEBUG_tracker:
            print(
                "Number of moving features before outlier removal: {}".format(
                    len(moving_feat)
                )
            )
        moving_feat = self.remove_outliers(moving_feat, outlier_dist)
        # Find bounding box, order: upper left, upper right, lower right, lower left
        box, center = self.find_bounding_box(moving_feat)
        box2, center2 = self.find_better_bounding_box(edgy_edges, box, center)

        # TODO new
        if cv2.norm(self.center) < cv2.norm(center) / 3:
            self.obj_in_frames = 0
        else:
            self.obj_in_frames += 1

        self.center = center

        # Draw bounding box
        if self.show_frames or self.save_video:
            painted_frame = self.draw_bounding_box(painted_frame, box, center)
            painted_frame = self.draw_bounding_box(
                painted_frame,
                box2,
                center2,
                box_color=(255, 0, 255),
                center_color=(255, 0, 0),
            )
        # Update variables
        self.prev_frame = gray
        self.prev_feat = moving_feat
        self.frames_processed_int = self.frames_processed_int + 1
        # Show the frame
        if self.show_frames:
            cv2.imshow("frame", painted_frame)
            keypress = cv2.waitKey(10)
        else:
            keypress = None

        if self.save_video:
            self.out_left.write(painted_frame)

        if keypress == 27:
            self.release()
        return keypress, box, center, self.prev_feat, frame

    def __del__(self):
        if self.save_video:
            self.out_left.release()
        if self.show_frames:
            # cv2.destroyWindow("frame")
            cv2.destroyAllWindows()

    # end of class


if __name__ == "__main__":
    # Load the video
    cap_left = cv2.VideoCapture(
        "../data/course/Stereo_conveyor_without_occlusions/left/left_%04d.png"
    )
    cap_right = cv2.VideoCapture(
        "../data/course/Stereo_conveyor_without_occlusions/right/right_%04d.png"
    )
    ret, frame = cap_left.read()
    if not ret:
        print("Left file not found")
        exit()
    # ret, frame[1] = cap_right.read()
    # if not ret:
    #     print("Right file not found")
    #     exit()
    print("Creating tracker object")
    print(frame.shape[:2])
    maps = rectify(frame.shape[:2], alpha=0)
    tracker = ObjectTracker(frame, show_frames=True, save_video=False)
    ret, frame = cap_left.read()

    mask = tracker.generate_mask(frame.copy())

    # ret, frame[0] = cap_left.read()
    # ret, frame[1] = cap_right.read()
    # frame0 = frame[0].copy()
    print("Begin tracking")
    loopcount = 0
    while ret:
        # print(loopcount)
        if loopcount < 50:
            # generates a better mask
            mask = cv2.bitwise_or(mask, tracker.generate_mask(frame.copy()))
            # cv2.imshow("test", mask)
            # cv2.waitKey()
        if loopcount % 10 == 0:
            make_new_points = True
        else:
            make_new_points = False
        keypress, box, center, moving_features, pictures = tracker.track(
            frame, get_new_features=make_new_points, mask=mask
        )
        ret, frame = cap_left.read()
        # ret, frame[0] = cap_left.read()
        # ret, frame[1] = cap_right.read()
        # frame[0] = frame[0] - frame0
        if keypress == 27:
            break
        if not tracker.show_frames:
            # print("Number of features: {}".format(len(moving_features)))
            print("Loopcount: {}".format(loopcount))
            cv2.imshow("left", pictures)
            # cv2.imshow("right", pictures[1])
            if cv2.waitKey(1) == 27:
                break
        loopcount = loopcount + 1
    print("Finished tracking, closing down")
    del tracker  # kinda important
    cv2.destroyAllWindows()  # Safety
