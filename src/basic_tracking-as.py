import numpy as np
import cv2 as cv2

cap = cv2.VideoCapture(
    "../data/course/sample_stereo_conveyor_without_occlusions/left/left_%04d.png"
)
_, prev_gray = cap.read()
prev_gray = cv2.cvtColor(prev_gray, cv2.COLOR_BGR2GRAY)
prev_feat = feat = cv2.goodFeaturesToTrack(
    prev_gray, maxCorners=300, qualityLevel=0.005, minDistance=10
)

while True:
    ret, frame = cap.read()
    if ret:
        frame2 = frame.astype(np.uint8)
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        feat, status, error = cv2.calcOpticalFlowPyrLK(prev_gray, gray, prev_feat, None)
        feats = []
        for i in range(len(prev_feat)):
            if cv2.norm(prev_feat[i, 0, :] - feat[i, 0, :]) > 1:
                cv2.line(
                    frame,
                    (prev_feat[i][0][0], prev_feat[i][0][1]),
                    (feat[i][0][0], feat[i][0][1]),
                    (0, 255, 0),
                    2,
                )
                cv2.circle(
                    frame, (prev_feat[i][0][0], prev_feat[i][0][1]), 5, (0, 255, 0), -1
                )
                feats.append(prev_feat[i])
        
        mean_feat = np.round(np.mean(feats, axis=0)).ravel()
        mean_feat = mean_feat.astype(np.uint32)
        marker = np.zeros_like(gray).astype(np.int32)
        marker[mean_feat[1], mean_feat[0]] = 1
        watershed = cv2.watershed(frame2, marker)
        
        frame2[watershed == -1] = [255, 0, 0]
        
        frame2_shape = np.shape(gray)
        mask = np.zeros((frame2_shape[0]+2, frame2_shape[1]+2))
        img_blur = cv2.blur(gray, (3,3))
        edges = cv2.Canny(img_blur, 50, 100, 3)
        mask[1:-1, 1:-1] = edges
        mask = mask.astype(np.uint8)
        
        rect = cv2.floodFill(frame2, mask, (mean_feat[0], mean_feat[1]), (255, 0, 0), 
                             loDiff = (3, 3, 3), upDiff = (3, 3, 3))
        
        for i in range(len(prev_feat)):
            if cv2.norm(prev_feat[i, 0, :] - feat[i, 0, :]) > 1:
                cv2.line(
                    frame2,
                    (prev_feat[i][0][0], prev_feat[i][0][1]),
                    (feat[i][0][0], feat[i][0][1]),
                    (0, 255, 0),
                    2,
                )
                cv2.circle(
                    frame2, (prev_feat[i][0][0], prev_feat[i][0][1]), 5, (0, 255, 0), -1
                )
        
        cv2.circle(
            frame2, (mean_feat[0], mean_feat[1]), 5, (0, 0, 255), -1
        )
        
        prev_gray = gray
        prev_feat = feat
        cv2.imshow("frame", frame2)
        if cv2.waitKey(1) == 27:
            break
    else:
        break

cap.release()
cv2.destroyAllWindows()
