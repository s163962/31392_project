import cv2
import numpy as np
from rectify import rectify
import basic_tracking
import open3d as o3d


def find_disparity(rectified_left, rectified_right):
    """
    Takes two rectified images, and tries to find a disparity map for them.
    """
    rect = [[], []]
    rect[0] = rectified_left
    rect[1] = rectified_right
    # https://github.com/opencv/opencv/blob/master/samples/python/stereo_match.py
    window_size = 3
    min_disp = 16
    num_disp = 112 - min_disp
    stereo1 = cv2.StereoSGBM_create(
        minDisparity=min_disp,
        numDisparities=num_disp,
        blockSize=16,
        P1=8 * 3 * window_size * 2,
        P2=32 * 3 * window_size * 2,
        disp12MaxDiff=1,
        uniquenessRatio=10,
        speckleWindowSize=100,
        speckleRange=32,
    )
    disparity1 = stereo1.compute(rect[0], rect[1]).astype(np.float32) / 16.0
    disp1 = (disparity1 - min_disp) / num_disp

    # cv2.imshow("disparity", (disparity - min_disp) / num_disp)
    stereo2 = cv2.StereoBM_create(numDisparities=272, blockSize=5)
    disparity2 = stereo2.compute(rect[0], rect[1])

    # both = np.hstack((frame[0], disparity))
    # cv2.imshow("disp", both)
    disparity1 = disparity1.astype("uint8")
    disparity2 = disparity2.astype("uint8")
    # disparity = disparity1
    # cv2.imshow("disp", disp1)
    # cv2.imshow("disparity1", disparity2)

    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return disp1


def absdiff(gray_img_1, gray_img_2):
    # Takes two bgr images and return the absolute difference in gray
    diff = np.abs(gray_img_1.astype(float) - gray_img_2.astype(float))
    return diff


def sumdiff(gray_img_1, gray_img_2):
    diff = absdiff(gray_img_1, gray_img_2)
    return np.sum(diff)


def spanmatch(match, span):
    # Returns the x coord of the best match, -1 is no result
    best_diff = sumdiff(match, span[:, 0 : match.shape[1]])
    best_x = -1
    for i in range(1, span.shape[1] - match.shape[1]):
        c_diff = sumdiff(match, span[:, i : (i + match.shape[1])])
        if c_diff < best_diff:
            best_diff = c_diff
            best_x = i
    return best_x


def reducedspanmatch(match, span, matchx, maxdisp, acceptable_likeness):
    # Returns the x coord of the best match, 0 is no result, only looks left
    # Maybe only works for match from right image
    best_diff = sumdiff(match, span[:, 0 : match.shape[1]])
    best_x = 0
    fromx = matchx - maxdisp
    if fromx < 0:
        fromx = 0
    tox = matchx
    for i in range(fromx, tox):
        c_diff = sumdiff(match, span[:, i : (i + match.shape[1])])
        if c_diff < acceptable_likeness:
            return i
        if c_diff < best_diff:
            best_diff = c_diff
            best_x = i
    return best_x


def cust_dispmap(img_left, img_right, blocksize=10, maxdisp=60, shift=15, pad=True):
    # takes img_left and img_right in gray. blocksize as int, maxdisp is guessed.
    # pad if ya like
    acceptable_likeness = 5
    if pad:
        img_left = np.pad(img_left, blocksize // 2, mode="edge")
        img_right = np.pad(img_right, blocksize // 2, mode="edge")
    # Shorthand
    bs = blocksize
    if shift < 0:
        shift = bs
    # Pre-allocate output matrix to same size as padded image
    dispmap = np.zeros_like(img_left)
    for j in range(0, img_right.shape[0] - bs, shift):
        # Slize a span
        span = img_right[j : j + bs, :]
        for i in range(0, span.shape[1] - bs, shift):
            # Slize a block
            block = img_left[j : j + bs, i : i + bs]
            # Map a [bs,bs] block in dispmap to how much you should move the block to match.
            temp = reducedspanmatch(block, span, i, maxdisp, acceptable_likeness)
            dispmap[j : j + bs, i : i + bs] = i - temp
    return dispmap


def areabased_dispmap(
    img_left, img_right, area, blocksize=10, maxdisp=60, shift=15, pad=True
):
    # takes img_left and img_right in gray. blocksize as int, maxdisp is guessed.
    # pad if ya like
    acceptable_likeness = 20
    if pad:
        img_left = np.pad(img_left, blocksize // 2, mode="edge")
        img_right = np.pad(img_right, blocksize // 2, mode="edge")
    # Shorthand
    bs = blocksize
    if shift < 0:
        shift = bs

    # Pre-allocate output matrix to same size as padded image
    dispmap = np.zeros_like(img_left)
    for j in range(int(area[0, 0]), int(area[1, 0]) - bs, shift):
        # Slize a span
        span = img_right[j : j + bs, :]
        for i in range(int(area[0, 1]), int(area[2, 1]) - bs, shift):
            # Slize a block
            block = img_left[j : j + bs, i : i + bs]
            # Map a [bs,bs] block in dispmap to how much you should move the block to match.
            temp = reducedspanmatch(block, span, i, maxdisp, acceptable_likeness)
            dispmap[j : j + bs, i : i + bs] = i - temp
    return dispmap


def trackbox2disparea(area):
    ymin = int(area[0, 0])
    ymax = int(area[1, 0])
    xmin = int(area[0, 1])
    xmax = int(area[2, 1])
    new_area = np.matrix([[xmin, ymin], [xmax, ymin], [xmax, ymax], [xmin, ymax]])
    return new_area


def test_areadispmap():
    file_path = "../data/course/sample_stereo_conveyor_without_occlusions"

    frame = [[], []]
    frame[0] = cv2.imread(file_path + "/left/left_0010.png", 0)  # cv2.IMREAD_GRAYSCALE)
    frame[1] = cv2.imread(
        file_path + "/right/right_0010.png", 0
    )  # cv2.IMREAD_GRAYSCALE,)

    maps, roi = rectify(frame[0].shape[:2], alpha=0)

    rect = [[], []]
    for i in range(len(rect)):
        rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)

    # find_disparity(rect[0], rect[1])
    # test: area = np.array([[350, 500], [600, 500], [600, 1200], [350, 1200]])
    # Original
    xmin = 573
    xmax = 739
    ymin = 374
    ymax = 536
    # New
    ymin = 573
    ymax = 739
    xmin = 374
    xmax = 536
    area = np.matrix([[xmin, ymin], [xmax, ymin], [xmax, ymax], [xmin, ymax]])
    center = (int((xmin + xmax) / 2), int((ymin + ymax) / 2))
    print(center)
    print("Finding disparity")
    # dispmap = cust_dispmap(
    #    rect[0], rect[1], blocksize=20, maxdisp=100, shift=15, pad=True
    # )
    dispmap = areabased_dispmap(
        rect[0], rect[1], area, blocksize=10, maxdisp=100, shift=3, pad=False
    )
    dispmap = cv2.circle(dispmap, (400, 400), 80, (255, 0, 0))
    # NOte to self, is this in (y,x) coords?????
    cv2.imshow("Custom dispmap", dispmap)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return


def test_track_pre_morten():
    cap_left = cv2.VideoCapture(
        "../data/course/sample_stereo_conveyor_without_occlusions/left/left_%04d.png"
    )
    cap_right = cv2.VideoCapture(
        "../data/course/sample_stereo_conveyor_without_occlusions/right/right_%04d.png"
    )
    ret, frame = cap_left.read()
    if not ret:
        print("Left file not found")
        exit()
    ret2, frame_right = cap_right.read()
    if not ret2:
        print("Right file not found")
        exit()

    print("Prepare rectification")
    maps, roi = rectify(frame.shape[:2], alpha=0)
    rect = [[], []]
    rect[0] = cv2.remap(frame, maps[0][0], maps[0][1], cv2.INTER_CUBIC)
    rect[1] = cv2.remap(frame_right, maps[1][0], maps[1][1], cv2.INTER_CUBIC)

    print("Creating tracker object")
    tracker = basic_tracking.ObjectTracker(rect[0], show_frames=True, save_video=False)
    ret, frame = cap_left.read()
    ret2, frame_right = cap_right.read()  # Always read the same amount of frames

    rect[0] = cv2.remap(frame, maps[0][0], maps[0][1], cv2.INTER_CUBIC)
    rect[1] = cv2.remap(frame_right, maps[1][0], maps[1][1], cv2.INTER_CUBIC)

    print("Begin tracking")
    loopcount = 0
    while ret:
        if loopcount % 10 == 0:
            make_new_points = True
        else:
            make_new_points = False
        keypress, box, center, moving_features, pictures = tracker.track(
            rect[0], get_new_features=make_new_points
        )
        # Testing areadisp, forgot gray
        grays = [[], []]
        grays[0] = cv2.cvtColor(rect[0], cv2.COLOR_BGR2GRAY)
        grays[1] = cv2.cvtColor(rect[1], cv2.COLOR_BGR2GRAY)

        area = trackbox2disparea(box)
        dispmap = areabased_dispmap(
            grays[0], grays[1], area, blocksize=15, maxdisp=100, shift=-1, pad=False
        )
        area_pixels = np.count_nonzero(dispmap)
        average_distance = np.sum(dispmap) / area_pixels
        print(average_distance)

        dispmap = cv2.circle(dispmap, center, 4, (155, 0, 0), 2)
        cv2.imshow("Custom dispmap", dispmap)

        ret, frame = cap_left.read()
        ret2, frame_right = cap_right.read()
        rect[0] = cv2.remap(frame, maps[0][0], maps[0][1], cv2.INTER_CUBIC)
        rect[1] = cv2.remap(frame_right, maps[1][0], maps[1][1], cv2.INTER_CUBIC)

        if keypress == 27:
            break
        if not tracker.show_frames:
            # print("Number of features: {}".format(len(moving_features)))
            print("Loopcount: {}".format(loopcount))
            cv2.imshow("left", pictures)
            # cv2.imshow("right", pictures[1])
            if cv2.waitKey(1) == 27:
                break
        loopcount = loopcount + 1
    print("Finished tracking, closing down")
    del tracker  # kinda important
    cv2.destroyAllWindows()  # Safety
    return


def builtintest():
    file_path = "../data/course/sample_stereo_conveyor_without_occlusions"

    frame = [[], []]
    frame[0] = cv2.imread(file_path + "/left/left_0010.png")
    frame[1] = cv2.imread(file_path + "/right/right_0010.png")

    maps, roi = rectify(frame[0].shape[:2], alpha=0)

    rect = [[], []]
    for i in range(len(rect)):
        rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)
    gray = [[], []]
    for i in range(len(rect)):
        gray[i] = cv2.cvtColor(rect[i], cv2.COLOR_BGR2GRAY)

    print("Finding disparity")
    disparity = find_disparity(gray[0], gray[1])

    cv2.imshow("Disparity", disparity)
    cv2.waitKey(0)


#    cv2.imshow("check", gray[1])
#   cv2.waitKey(0)
def built_in_plus_tracker():
    cap_left = cv2.VideoCapture(
        "../data/course/sample_stereo_conveyor_without_occlusions/left/left_%04d.png"
    )
    cap_right = cv2.VideoCapture(
        "../data/course/sample_stereo_conveyor_without_occlusions/right/right_%04d.png"
    )

    print("Creating stereo object")
    # https://github.com/opencv/opencv/blob/master/samples/python/stereo_match.py
    # block_size = 5
    # min_disp = 16 * 0
    # num_disp = 16 * 12 - min_disp
    # stereo = cv2.StereoSGBM_create(
    #     minDisparity=min_disp,
    #     numDisparities=num_disp,
    #     blockSize=block_size,
    #     P1=8 * 3 * block_size ** 2,
    #     P2=32 * 3 * block_size ** 2,
    #     disp12MaxDiff=1,  # 10,
    #     uniquenessRatio=10,  # 1,
    #     speckleWindowSize=100,  # 100,
    #     speckleRange=32,  # 32,
    #     # mode=cv2.STEREO_SGBM_MODE_HH,
    # )
    # stereo = cv2.StereoSGBM_create(blockSize=15)
    # stereo.setNumDisparities(16 * 14)
    stereo = cv2.StereoBM_create(numDisparities=16 * 14, blockSize=7)
    # stereo.setMode(cv2.STEREO_SGBM_MODE_HH)  # No noteworthy difference
    # take onne image, make sliders for different values

    print("Creating tracker")
    frame = [[], []]
    ret, frame[0] = cap_right.read()
    ret, frame[1] = cap_right.read()
    maps, roi, _ = rectify(frame[0].shape[:2], alpha=0)
    rect = [[], []]
    for i in range(len(rect)):
        rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)
    if not ret:
        print("no im")
        exit()
    # tracker = basic_tracking.ObjectTracker(rect[0])

    while ret:
        gray = [[], []]
        blur = [[], []]
        for i in range(len(rect)):
            rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)
            gray[i] = cv2.cvtColor(rect[i], cv2.COLOR_BGR2GRAY)
            blur[i] = cv2.GaussianBlur(rect[i], (7, 7), sigmaX=0)

        # box, center, features, picture = tracker.track(rect[0], get_new_features=newf)
        # When disping:
        # gray[0] = cv2.cvtColor(rect[0], cv2.COLOR_BGR2GRAY)
        # gray[1] = cv2.cvtColor(rect[1], cv2.COLOR_BGR2GRAY)
        disparity = stereo.compute(gray[0], gray[1])  # .astype(np.float32) / 16.0
        disparity = disparity / np.max(disparity)  # Remap to [0,1]
        # sigma = 400 # For bilat
        # bilat = cv2.bilateralFilter(disparity.astype(np.float32), 5, sigma, sigma)

        cv2.imshow("Disp", disparity)

        ret, frame[0] = cap_left.read()
        _, frame[1] = cap_right.read()
        keypress = cv2.waitKey(1)
        if keypress == 27:
            break


if __name__ == "__main__":
    # test_track_pre_morten()
    # test_areadispmap()
    # builtintest()
    built_in_plus_tracker()
