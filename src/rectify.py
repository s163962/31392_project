import cv2
import pickle


def rectify(image_size, new_image_size=0, fisheye=0, alpha=0, save=0, extra_out=False):
    print(f"Starting Camera Rectification")
    print(f"=============================")
    # print("Loading calibration intrinsics...")
    # with open("pickle/calib_intrin.pkl", "rb") as f:
    #     mtx, dist = pickle.load(f)

    # print("Loading calibration extrinsics...")
    # with open("pickle/calib_extrin.pkl", "rb") as f:
    #     R, T, _, _ = pickle.load(f)

    print("Loading stereo parameters...")
    try:
        with open("src/pickle/calib_stereo.pkl", "rb") as f:
            mtx, dist, R, T, _, _ = pickle.load(f)
    except:
        with open("pickle/calib_stereo.pkl", "rb") as f:
            mtx, dist, R, T, _, _ = pickle.load(f)

    print("Calculating rectification transformations...")
    # new_image_size = image_size[::-1]  # (1500, 500)
    if new_image_size == 0:
        new_image_size = image_size[::-1]
    if not fisheye:
        R1, R2, P1, P2, Q, roi_left, roi_right = cv2.stereoRectify(
            mtx[0],
            dist[0],
            mtx[1],
            dist[1],
            image_size[::-1],
            R,
            T,
            alpha=alpha,
            newImageSize=new_image_size,
        )

        map_left = cv2.initUndistortRectifyMap(
            mtx[0], dist[0], R1, P1, new_image_size, cv2.CV_32FC1
        )
        map_right = cv2.initUndistortRectifyMap(
            mtx[1], dist[1], R2, P2, new_image_size, cv2.CV_32FC1
        )
    else:
        R1, R2, P1, P2, Q = cv2.fisheye.stereoRectify(
            mtx[0],
            dist[0],
            mtx[1],
            dist[1],
            image_size[::-1],
            R,
            T,
            flags=0,
            newImageSize=new_image_size,
        )

        map_left = cv2.fisheye.initUndistortRectifyMap(
            mtx[0], dist[0], R1, P1, new_image_size, cv2.CV_32FC1
        )
        map_right = cv2.fisheye.initUndistortRectifyMap(
            mtx[1], dist[1], R2, P2, new_image_size, cv2.CV_32FC1
        )

        roi_left = (0, 0, new_image_size[0], new_image_size[1])
        roi_right = (0, 0, new_image_size[0], new_image_size[1])

        Q = None

    if save:
        print("Saving to pickle...")
        with open("src/pickle/" + save + ".pkl", "wb") as f:
            pickle.dump([map_left, map_right], f)
    print("Done")

    if extra_out:
        return [map_left, map_right], [roi_left, roi_right], [R1, R2], [P1, P2], Q
    else:
        return [map_left, map_right], [roi_left, roi_right], Q


if __name__ == "__main__":
    file_path = "data/course/sample_stereo_conveyor_without_occlusions"
    save = True
    alpha = 1

    frame = [[], []]
    frame[0] = cv2.imread(file_path + "/left/left_0000.png")
    frame[1] = cv2.imread(file_path + "/right/right_0000.png")

    maps, roi, _ = rectify(frame[0].shape[:2], alpha=alpha, fisheye=0)

    rect = [[], []]
    rect[0] = cv2.remap(frame[0], maps[0][0], maps[0][1], cv2.INTER_CUBIC)
    rect[1] = cv2.remap(frame[1], maps[1][0], maps[1][1], cv2.INTER_CUBIC)

    if save:
        cv2.imwrite(f"fig/rect/rect_left_alpha{alpha}.png", rect[0])
        cv2.imwrite(f"fig/rect/rect_right_alpha{alpha}.png", rect[1])

    for i in range(len(rect)):
        cv2.rectangle(frame[i], roi[i][:2], roi[i][-2:], color=(0, 255, 0), thickness=2)
        cv2.rectangle(rect[i], roi[i][:2], roi[i][-2:], color=(0, 255, 0), thickness=2)

    cv2.imshow("left original", frame[0])
    cv2.imshow("right original", frame[1])
    cv2.imshow("left rectified", rect[0])
    cv2.imshow("right rectified", rect[1])

    print(roi)

    if cv2.waitKey(0) == 27:
        cv2.destroyAllWindows()
