# import numpy as np
import cv2
from matplotlib import pyplot as plt

def steCalib(K1, K2):
    cv2.stereoCalibrate()
    cv2.stereoRectify()
    pass

def getDisparity(imgL, imgR):
    #stereo = cv2.createStereoBM(numDisparities=16, blockSize=15)
    stereo = cv2.StereoBM_create(numDisparities=16, blockSize=15)
    imgL = cv2.cvtColor(imgL,cv2.COLOR_BGR2GRAY)
    imgR = cv2.cvtColor(imgR,cv2.COLOR_BGR2GRAY)
    disparity = stereo.compute(imgL, imgR)
    #plt.imshow(disparity, "gray")
    cv2.imshow('disp', disparity)
    cv2.waitKey(2000)
    cv2.destroyAllWindows()
    #plt.show()
    return


# Test
imgL = cv2.imread(
    "../data/course/sample_stereo_conveyor_without_occlusions/left/left_0129.png"
)

if imgL is None:
    imgL = cv2.imread('/home/navn/31392_project/data/course/sample_stereo_conveyor_without_occlusions/left/left_00129.png')
imgR = cv2.imread(
    "../data/course/sample_stereo_conveyor_without_occlusions/right/right_0129.png"
)
if imgR is None:
    imgR = cv2.imread('/home/navn/31392_project/data/course/sample_stereo_conveyor_without_occlusions/right/right_00129.png')
print(imgR.shape)
print(imgL.shape)

getDisparity(imgL, imgR)
