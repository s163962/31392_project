import cv2
import numpy as np
import pickle
from rectify import rectify


def drawlines(img1, img2, lines, pts1, pts2):
    """ img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines """
    r, c = img1.shape[:2]
    # img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
    # img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        x0, y0 = map(int, [0, -r[2] / r[1]])
        x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])
        img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 2)
        img1 = cv2.circle(img1, tuple(pt1.squeeze()), 5, color, -1)
        img2 = cv2.circle(img2, tuple(pt2.squeeze()), 5, color, -1)
    return img1, img2


def epipolar(img_num, type=0, alpha=0, save=0):
    if type == 0:  # chessboard
        pattern_size = (9, 6)
        filepath = "data/course/stereo_calibration_images/"
        frame = [[], []]
        frame[0] = cv2.imread(filepath + "left_" + img_num + ".png")
        frame[1] = cv2.imread(filepath + "right_" + img_num + ".png")
    else:
        filepath = "data/course/sample_stereo_conveyor_without_occlusions/"
        frame = [[], []]
        frame[0] = cv2.imread(filepath + "left/left_" + img_num + ".png")
        frame[1] = cv2.imread(filepath + "right/right_" + img_num + ".png")

    with open("src/pickle/calib_stereo.pkl", "rb") as f:
        _, _, _, _, _, F = pickle.load(f)

    maps, _, _ = rectify(frame[0].shape[:2], alpha=alpha)
    rect = [[], []]
    for i in range(len(rect)):
        rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)
        # rect[i] = cv2.resize(rect[i].copy(), None, fx=resize_factor, fy=resize_factor)

    if type == 0:
        pts = [[], []]
        for i in range(len(pts)):
            ret, pts[i] = cv2.findChessboardCorners(rect[i], pattern_size)
            pts[i] = pts[i].astype(np.int32)
    else:
        sift = cv2.xfeatures2d.SIFT_create()
        kp1, des1 = sift.detectAndCompute(rect[0], None)
        kp2, des2 = sift.detectAndCompute(rect[1], None)

        bf = cv2.BFMatcher()
        matches = bf.match(des1, des2)

        matches = sorted(matches, key=lambda x: x.distance)
        nb_matches = 50

        good = []
        pts = [[], []]
        for m in matches[:nb_matches]:
            good.append(m)
            pts[0].append(kp1[m.queryIdx].pt)
            pts[1].append(kp2[m.trainIdx].pt)
        pts[0] = np.int32(pts[0])
        pts[1] = np.int32(pts[1])

    lines1 = cv2.computeCorrespondEpilines(pts[1], 2, F)
    # lines1 = cv2.computeCorrespondEpilines(corners[0].astype(np.int32), 2, F)
    lines1 = lines1.reshape(-1, 3)
    new_l, _ = drawlines(rect[0], rect[1], lines1, pts[0], pts[1])

    lines2 = cv2.computeCorrespondEpilines(pts[0], 1, F)
    # lines2 = cv2.computeCorrespondEpilines(corners[1].astype(np.int32), 1, F)
    lines2 = lines2.reshape(-1, 3)
    new_r, _ = drawlines(rect[1], rect[0], lines2, pts[1], pts[0])

    if save:
        cv2.imwrite(f"fig/epi/epi_left_alpha{alpha}.png", new_l)
        cv2.imwrite(f"fig/epi/epi_right_alpha{alpha}.png", new_r)
    cv2.imshow("left", new_l)
    cv2.imshow("right", new_r)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    epipolar("0027", type=0, alpha=1, save=1)
