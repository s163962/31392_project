import numpy as np
import cv2 as cv2


def getK():
    K_left = np.array(
        [[701.55000951, 0, 620.13341806], [0, 701.51162542, 369.99010521], [0, 0, 1]]
    )
    K_right = np.array(
        [[699.32072666, 0, 649.04165171], [0, 699.6672647, 374.68704285], [0, 0, 1]]
    )

    return K_left, K_right


def getLeftImage(i):
    return cv2.imread("left/{0:010d}.png".format(i), 0)


def getRightImage(i):
    return cv2.imread("right/{0:010d}.png".format(i), 0)
