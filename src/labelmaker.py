import numpy as np
import os
import cv2
import math
import sys


"""def method0(imgs, imgPrev):

    # params for ShiTomasi corner detection
    feature_params = dict(maxCorners=100, qualityLevel=0.3, minDistance=7, blockSize=7)

    # Parameters for lucas kanade optical flow
    lk_params = dict(
        winSize=(15, 15),
        maxLevel=2,
        criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03),
    )

    # Create some random colors
    color = np.random.randint(0, 255, (100, 3))

    # Take first frame and find corners in it
    old_frame = imgPrev
    old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
    p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)

    # Create a mask image for drawing purposes
    mask = np.zeros_like(old_frame)
    for i in range(1, len(imgs)):
        frame = cv2.imread(path + book + imgs[i])

        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # calculate optical flow
        p1, st, err = cv2.calcOpticalFlowPyrLK(
            old_gray, frame_gray, p0, None, **lk_params
        )

        # Select good points
        good_new = p1[st == 1]
        good_old = p0[st == 1]

        # draw the tracks
        for i, (new, old) in enumerate(zip(good_new, good_old)):
            a, b = new.ravel()
            c, d = old.ravel()
            mask = cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2)
            frame = cv2.circle(frame, (a, b), 5, color[i].tolist(), -1)
        img = cv2.add(frame, mask)

        cv2.imshow("Matching", img)
        k = cv2.waitKey(50) & 0xFF
        if k == 27:
            break

        # Now update the previous frame and previous points
        old_gray = frame_gray.copy()
        p0 = good_new.reshape(-1, 1, 2)"""


def cropFromMask(imgPrev, mask):
    ret, thresh = cv2.threshold(mask, 2, 255, 0)
    pack = cv2.findContours(thresh, 1, 2)
    if len(pack) > 2:
        _, contours, hierarchy = pack
    else:
        contours, hierarchy = pack

    bestCntIdx = 0
    i = 0
    # print(contours)

    for cnt in contours:
        area = cv2.contourArea(cnt)
        bestArea = cv2.contourArea(cnt)
        # print(area, bestArea)
        if area > bestArea:
            cnt = cnt
        i += 1

    # cv2.drawContours(imgPrev, [cnt], -1, (0, 0, 255), 2)
    # cv2.imshow("Keypoints in new image", imgPrev)
    # cv2.waitKey()

    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    # cv2.drawContours(imgPrev, [box], -1, (0, 0, 255), 2)
    # cv2.imshow("Keypoints in new image", imgPrev)
    # cv2.waitKey()
    # print(type(box), type(rect))
    # print(box, rect)
    # print(imgPrev.shape)
    imgPrevCrop, box_coords = cropAndTransform(imgPrev, box, 0)
    # print(imgPrevCrop.shape)
    # cv2.imshow("Matching", imgPrevCrop)
    # cv2.imshow("cropped image with keypoints", imgPrev)
    rect_simple = cv2.boundingRect(cnt)
    return imgPrevCrop, rect_simple


def cropAndTransform(img, box, offset=-180):
    if len(box) == 4:
        # print(box)
        h = int(math.sqrt((box[0, 0] - box[1, 0]) ** 2 + (box[0, 1] - box[1, 1]) ** 2))
        w = int(math.sqrt((box[1, 0] - box[2, 0]) ** 2 + (box[1, 1] - box[2, 1]) ** 2))
        # print(w, h)
        cx = int(np.sum(box[:, 0]) / 4)
        cy = int(np.sum(box[:, 1]) / 4)

        y = cy - int(h / 2)
        x = cx - int(w / 2)
        # print("test", w, h)

        # imgRotated = cv2.warpAffine(img, M, (0, 0))
    else:

        # print("box:", box)
        x = box[0][0]
        y = box[0][1]
        w = int(box[1][0] - x)
        h = int(box[1][1] - y)
        # print("before:", x, y, w, h)

    # print("test", w, h)
    y, x = (y - 5, x - 10)
    w, h = (w + 15, h + 15)
    if y < 0:
        y = 0
    if x < 0:
        x = 0
    # if y + h >= img.shape[1]:
    #     h = img.shape[1] - y - 1
    # if x + w >= img.shape[0]:
    #     w = img.shape[0] - x - 1
    # Upper limits are limited by the slicing

    # print("after:", x, y, w, h)
    # print("test", w, h)
    if w > h * 5 or h > w * 5:
        return None, None
    if w < 80 or h < 80:
        return None, None
    # print(x, y, w, h)
    imgCrop = img[y : y + h, x : x + w]

    # img3 = img3[br_y:tl_y, br_x:tl_x]
    # dst = np.array([[0, 0], [w - 1, 0], [w - 1, h - 1], [0, h - 1]], dtype="float32",)
    # # compute the perspective transform matrix and then apply it
    # M = cv2.getPerspectiveTransform(box.astype(np.float32), dst)
    # warped = cv2.warpPerspective(imgPrev, M, (w, h))

    # Draws the box
    # cv2.drawContours(imgPrev, [box], 0, (0, 0, 255), 2)

    # cv2.imshow("Matching", imgCrop)
    # cv2.waitKey()
    return imgCrop, (x, y, w, h)
    # return warped


def findBox(img1, img2, mask, kp1, kp2, good):
    # https://stackoverflow.com/questions/51606215/how-to-draw-bounding-box-on-best-matches
    matches = sorted(good, key=lambda x: x[0].distance)

    good_matches = matches[:20]

    src_pts = np.float32([kp1[m[0].queryIdx].pt for m in good_matches]).reshape(
        -1, 1, 2
    )
    dst_pts = np.float32([kp2[m[0].trainIdx].pt for m in good_matches]).reshape(
        -1, 1, 2
    )
    # print(src_pts.shape, dst_pts.shape)

    img_temp1 = img1.copy()
    img_temp2 = img2.copy()

    for center in dst_pts:
        cv2.circle(img_temp2, tuple(center[0].astype(np.int)), 5, (255), thickness=5)
        cv2.circle(img_temp2, tuple(center[0].astype(np.int)), 3, (0), thickness=5)
    for center in src_pts:
        cv2.circle(img_temp1, tuple(center[0].astype(np.int)), 5, (255), thickness=5)
        cv2.circle(img_temp1, tuple(center[0].astype(np.int)), 3, (0), thickness=5)

    M, _ = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    # M, _ = cv2.getAffineTransform(src_pts, dst_pts)
    # print(dst_pts, mask)
    if M is not None:
        h, w = img1.shape[:2]
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(
            -1, 1, 2
        )
        dst = cv2.perspectiveTransform(pts, M)
        # dst = cv2.warpPerspective(pts, M)
        # print(pts, dst_pts)

        dst += (w, 0)  # adding offset
        box = (dst).reshape(-1, 2) - (w, 0)
        # print(box, dst)
        if img1 is None or img2 is None or kp1 is None or kp2 is None or good is None:
            print("Something wrong inside findbox, but i don't know what...")
            return None, None, None
        if len(img1) < 1 or len(img2) < 1:
            print("image is not there?", img1.shape, img2.shape)
            return None, None, None

        img3 = cv2.drawMatchesKnn(
            img1,
            kp1,
            img2,
            kp2,
            good,
            None,
            flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS,
        )
        # Draw bounding box in Red
        img3 = cv2.polylines(img3, [np.int32(dst)], True, (0, 0, 255), 3, cv2.LINE_AA)
        # cv2.drawContours(img3, [np.int32(dst)], 0, (0, 0, 255), 2)

        # print(box)

        img_cropped, box_coords = cropAndTransform(img2, box)
        mask_cropped, _ = cropAndTransform(mask, box)

        if img_cropped is None:
            print("Unreasonable ratios", box)
            return None, None, None
        # test = img_cropped.copy()
        # test[255 - mask_cropped == 255] = 255

        # cv2.imshow("cropped image with keypoints", img2)
        # print(img3.shape, img2.shape)
        # cv2.imshow("Keypoints in new image", img1)
        # cv2.imshow("Keypoints in new image", cv2.drawKeypoints(img1, kp1, img1))
        # cv2.waitKey()
        # cv2.imshow("cropped image with keypoints", cv2.drawKeypoints(img2, kp2, img2))
        # cv2.imshow("cropped image with keypoints", img2)
        # cv2.waitKey()
        # cv2.imshow("Matching", test)
        # cv2.imshow("cropped image with keypoints", img_cropped)
        cv2.imshow("Matching", img3)
        # cv2.waitKey()
        # print("before return it had this hape:", cropped.shape)
        # print(dst)
        # cv2.waitKey()
        return img_cropped, mask_cropped, box_coords
    else:
        print("M is None in findHomography")
        return None, None, None


def findGoodPoints(bf, des_prev, des, tolerance=0.4):
    matches = bf.knnMatch(des_prev, des, k=2)
    good = []
    for m, n in matches:
        if m.distance < tolerance * n.distance:
            good.append([m])
            # good_prev.append([m])
            # print(i, m)
            # print(kp2[m.queryIdx].pt)
            # print(kp[n.queryIdx].pt).
    good_points = len(good)
    return good, good_points


def lineExtraction(
    img,
    gray,
    mask=None,
    rho=1,
    theta=np.pi / 180,
    threshold=50,
    min_line_length=470,
    max_line_gap=50,
    thickness=0,
):
    if img.shape[0] == 720:
        blur_gray = gray
        kernel_size = 31
        blur_gray = cv2.medianBlur(blur_gray, kernel_size)
        kernel_size = 19
        blur_gray = cv2.GaussianBlur(blur_gray, (kernel_size, kernel_size), 0)
        low_threshold = 5
        high_threshold = 20

        min_line_length = min_line_length / 1.5
        max_line_gap = max_line_gap / 1.5

    else:
        kernel_size = 7
        blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)
        low_threshold = 25
        high_threshold = 75
    edges = cv2.Canny(blur_gray, low_threshold, high_threshold)
    if img.shape[0] == 720:
        kernel = np.ones((3, 3), np.uint8)
        edges = cv2.dilate(edges, kernel, iterations=1)
    if mask is not None:
        edges[255 - mask == 255] = 0
        # cv2.imshow("Matching", mask)
        # cv2.waitKey(200)
    # rho = 1  # distance resolution in pixels of the Hough grid
    # theta = np.pi / 180  # angular resolution in radians of the Hough grid
    # threshold = 50  # minimum number of votes (intersections in Hough grid cell)
    # min_line_length = 470  # minimum number of pixels making up a line
    # max_line_gap = 50  # maximum gap in pixels between connectable line segments
    line_image = np.copy(gray) * 0  # creating a blank to draw lines on
    # cv2.imshow("Matching", edges)
    # cv2.waitKey(200)
    # Run Hough on edge detected image
    # Output "lines" is an array containing endpoints of detected line segments
    lines = cv2.HoughLinesP(
        edges, rho, theta, threshold, np.array([]), min_line_length, max_line_gap
    )
    if drawDebugImgs:
        cv2.imshow("lines", edges)
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                cv2.line(line_image, (x1, y1), (x2, y2), (255), thickness)
        # return line_image
        # return cv2.addWeighted(img, 0.8, line_image, 1, 0)

    return 255 - line_image


def removeOutlierKeypoints(kp, des, corner):
    x_cen = 0
    y_cen = 0
    ## these gives out the indexes of src and dst poitns
    # print(
    #    good[10][0].queryIdx, good[10][0].trainIdx,
    # )
    for p in kp:
        x_cen += p.pt[0]
        y_cen += p.pt[1]
    centroid = (corner[1] / 2, corner[0] / 2)  # (x_cen / len(kp), y_cen / len(kp))
    distances = np.zeros(len(kp))
    # print(centroid, (x_cen / len(kp), y_cen / len(kp)))

    for i in range(len(kp)):
        p = kp[i]
        distances[i] = math.sqrt(
            (p.pt[0] - centroid[0]) ** 2 + (p.pt[1] - centroid[1]) ** 2
        )

    # print(distances)
    if distances is None or len(distances) == 0:
        return None, None
    mu, sigma = cv2.meanStdDev(distances)
    if mu is None or sigma is None:
        return None, None
    # print(distances, (mu + 2 * sigma))
    # print(centroid, mu, sigma)
    indexes = []
    # print(len(kp))
    for i in range(len(kp) - 1, 0, -1,):
        if distances[-i] > (mu + 1.5 * sigma):
            indexes.append(i)
            del kp[i]
    if des is not None:
        des_new = np.delete(des, indexes, 0)
    else:
        des_new = None
    # print(len(kp))

    # print(centroid)
    # print(type(des), type(des_new))
    return kp, des_new


def write_to_file(img, path, i, top_left, bot_right):
    top_left = (top_left[0] - 10, top_left[1] - 10)
    bot_right = (bot_right[0] + 30, bot_right[1] + 10)

    cx = ((top_left[0] + bot_right[0]) / 2) / img.shape[1]
    cy = ((top_left[1] + bot_right[1]) / 2) / img.shape[0]

    w = (bot_right[0] - top_left[0]) / img.shape[1]
    h = (bot_right[1] - top_left[1]) / img.shape[0]
    # leading zeros paa folders

    print(cx, cy, w, h)

    filename = "{0:05d}{1:s}".format(i, ".txt")
    print(filename)
    label_path = path + str(bookid) + "/"
    label_file = open(label_path + filename, "w")
    label_file.write(f"{type} {cx} {cy} {w} {h}")
    label_file.close()

    cv2.rectangle(img, top_left, bot_right, (255, 0, 0), 2)
    cv2.imshow("labeled image", img)


def method1(imgs, img, imgPrevAlpha, path, book, bookid):
    print(img.shape)
    img_prev, rect = cropFromMask(img, imgPrevAlpha)
    print(rect)
    print(img_prev.shape)
    top_left = tuple(rect[0:2])
    bot_right = (rect[2] + rect[0], rect[3] + rect[1])
    write_to_file(img, path, 0, top_left, bot_right)

    img_gray_prev_cropped = cv2.cvtColor(img_prev, cv2.COLOR_BGR2GRAY)

    sift = cv2.xfeatures2d.SIFT_create()
    kp_prev, des_prev = sift.detectAndCompute(img_gray_prev_cropped, None)
    print(img_prev.shape)
    # img_prev = cv2.drawKeypoints(img_gray_prev_cropped, kp, img_prev)
    bf = cv2.BFMatcher()

    good_points = 0
    if img.shape[0] == 720:
        emergency_tolerance = 150
        decaying_emergency_tolerance = emergency_tolerance
        decay = 3
        minimum_emergency_tolerance = 95
        new_points_tolerance = 25
        good_points_tolerance = 15
        min_line_length1 = 1080
        max_line_gap1 = 50
        min_line_length2 = 15
        max_line_gap2 = 40
        thickness1 = 50
        thickness2 = 100
        good_point_distance = 0.95
        emergency_good_point_distance = 0.8
    else:
        emergency_tolerance = 150
        decaying_emergency_tolerance = emergency_tolerance
        decay = 3
        minimum_emergency_tolerance = 95
        new_points_tolerance = 25
        good_points_tolerance = 20
        min_line_length1 = 470
        max_line_gap1 = 50
        min_line_length2 = 80
        max_line_gap2 = 80
        thickness1 = 50
        thickness2 = 100
        good_point_distance = 0.8
        emergency_good_point_distance = 0.8

    kp_emergency, des_emergency, img_gray_emergency = (
        kp_prev,
        des_prev,
        img_gray_prev_cropped,
    )

    boxes_labeled = 0

    for i in range(1, len(imgs)):
        img = cv2.imread(path + book + imgs[i])
        if img is None:
            print(path + book + imgs[i] + "couldn't be loaded")
            continue
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # optimize first mask
        mask = lineExtraction(
            img,
            img_gray,
            None,
            min_line_length=min_line_length1,
            max_line_gap=max_line_gap1,
            thickness=thickness1,
        )
        mask2 = lineExtraction(
            img,
            img_gray,
            mask,
            min_line_length=min_line_length2,
            max_line_gap=max_line_gap2,
            thickness=thickness2,
        )
        # cv2.imshow("cropped image with keypoints", mask)
        # cv2.waitKey()
        mask = 255 - mask2
        # cv2.imshow("Matching", mask)
        # print(img_gray_prev_cropped.shape)
        if drawDebugImgs:
            cv2.imshow("Mask", cv2.addWeighted(img_gray, 0.8, 255 - mask2, 1, 0))

        kp, des = sift.detectAndCompute(img_gray, mask)

        if True:
            cv2.imshow(
                "Keypoints in new image", cv2.drawKeypoints(img_gray, kp, img_gray)
            )
            cv2.waitKey(1)

        if len(kp) > 30:
            print(
                f"image {i-1} of {len(imgs)}, labeled: {boxes_labeled}. Has          : kps on big image: {len(kp)}, good points: {good_points}, kps on small image: {len(kp_prev)}"
            )
            good, good_points = findGoodPoints(
                bf, des_prev, des, tolerance=good_point_distance
            )
            if good is None or mask is None or kp is None or img_gray is None:
                continue
            if good_points > good_points_tolerance:
                img_gray_prev_cropped_temp, mask_temp, box = findBox(
                    img_gray_prev_cropped, img_gray, mask, kp_prev, kp, good
                )

            else:
                good, good_points = findGoodPoints(
                    bf, des_emergency, des, tolerance=emergency_good_point_distance
                )
                if good is None or mask is None or kp is None or img_gray is None:
                    continue
                print(
                    f"image {i} of {len(imgs)}, labeled: {boxes_labeled}. Has these new: kps on big image: {len(kp)}, good points: {len(good)}, kps on small image: {len(kp_prev)}"
                )
                if good_points <= good_points_tolerance:
                    continue
                img_gray_prev_cropped_temp, mask_temp, box = findBox(
                    img_gray_emergency, img_gray, mask, kp_emergency, kp, good
                )
            if img_gray_prev_cropped_temp is None:
                print("img_gray_prev_cropped_temp is empty")
                continue
            elif (
                img_gray_prev_cropped_temp.shape[0] == 0
                or img_gray_prev_cropped_temp.shape[1] == 0
            ):
                print(
                    "img_gray_prev_cropped_temp is faulty",
                    img_gray_prev_cropped_temp.shape,
                )
                continue
            else:
                img_gray_prev_cropped = img_gray_prev_cropped_temp

            if good_points > new_points_tolerance:
                boxes_labeled += 0
                # box_hash["i"] = box
                # top_left = tuple(np.amin(box, 0).astype(np.int))
                # bot_right = tuple(np.amax(box, 0).astype(np.int))
                top_left = (box[0], box[1])
                bot_right = (box[0] + box[2], box[1] + box[3])
                print(top_left, bot_right)
                write_to_file(img, path, i, top_left, bot_right)
                kp = sift.detect(img_gray_prev_cropped, mask_temp)
                # kp, des = sift.detectAndCompute(img_gray_prev_cropped, mask_temp)
                corner = img_gray_prev_cropped.shape
                kp_temp, _ = removeOutlierKeypoints(kp, None, corner)
                if kp_temp is None:
                    print("kp_temp is empty")
                    continue
                kp = kp_temp
                positions = []
                for p in kp:
                    positions.append(list(p.pt))
                positions = np.array(positions)
                tl = tuple(np.amin(positions, 0).astype(np.int))
                br = tuple(np.amax(positions, 0).astype(np.int))

                # used to refine the crop.

                # tl = (top_left[0] - box[0], top_left[1] - box[1])
                # br = (bot_right[0] - box[0], bot_right[1] - box[1])
                box_refined = [(tl[0] - 50, tl[1] - 50), (br[0] + 50, br[1] + 50)]

                # top_left = tuple(np.amin(box, 0).astype(np.int))
                # bot_right = tuple(np.amax(box, 0).astype(np.int))

                img_temp1 = img_gray_prev_cropped.copy()

                # cv2.circle(img_temp1, tl, 5, (255), thickness=10)
                # cv2.circle(img_temp1, br, 5, (255), thickness=10)
                # cv2.imshow("Keypoints in new image", img_temp1)
                # cv2.waitKey()

                # TODO Find a way to crop the original image rather than the cropped one!
                img_gray_prev_cropped, box_coords = cropAndTransform(
                    img_gray_prev_cropped, box_refined
                )
                mask_temp, _ = cropAndTransform(mask_temp, box_refined)

                # cv2.imshow("Keypoints in new image", img_temp2)
                # cv2.waitKey()
                # Needs to find the descriptors ised to find the good matches
                if img_gray_prev_cropped is not None and mask_temp is not None:
                    kp, des = sift.detectAndCompute(img_gray_prev_cropped, mask_temp)
                else:
                    continue
                kp_prev = kp
                des_prev = des
                if drawDebugImgs:
                    cv2.imshow(
                        "cropped image with keypoints",
                        cv2.drawKeypoints(
                            img_gray_prev_cropped, kp, img_gray_prev_cropped
                        ),
                    )
                # cv2.waitKey()
            decaying_emergency_tolerance -= decay
            if (
                good_points > decaying_emergency_tolerance
                and good_points > minimum_emergency_tolerance
            ):
                decaying_emergency_tolerance = emergency_tolerance
                kp_emergency, des_emergency, img_gray_emergency = (
                    kp,
                    des,
                    img_gray_prev_cropped,
                )
                print("New good points!")

            if (
                good_points > good_points_tolerance
                and good_points <= new_points_tolerance
            ):
                if drawDebugImgs:
                    cv2.imshow(
                        "cropped image with keypoints",
                        cv2.drawKeypoints(
                            img_gray_emergency, kp_emergency, img_gray_emergency
                        ),
                    )

        else:
            print("not enough keypoints to track")
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break


## TODO make a argument for debuging!
drawDebugImgs = True
bookid = "box00001"
type = 269
bookpath = "box"


print("This is the name of the program:", sys.argv[0])

print("Argument List:", str(sys.argv))

valid_types = [269, 69, 56]


if (
    any([arg == "-h" for arg in sys.argv])
    or len(sys.argv) != 4
    or not sys.argv[1].isnumeric()
    or not any([sys.argv[1] == str(objtype) for objtype in valid_types])
    or not sys.argv[3][-5:].isnumeric()
):
    print(
        "\n\nUsage:\n python <filename> <object type> <folder prop type> <folder (id)>"
    )
    print("Example:\n python src/labelmaker.py 69 train/book book00000")
    print("\n\n")
    print("box = 269, book = 69, coffee cup = 56")
    exit()
else:
    type = sys.argv[1]
    bookpath = sys.argv[2]
    bookid = sys.argv[3]

type = 69
bookpath = "train/book"
bookid = "book00000"

book = str(bookid) + "/"
path = "data/" + str(bookpath) + "/"
print(path + book)
imgs = sorted(os.listdir(path + book))
for string in imgs:
    if string[-4:] == ".txt":
        imgs.remove(string)

# img(b,g,r,alpha)
imgPrev = cv2.imread(path + "init_" + str(bookid) + ".png", cv2.IMREAD_UNCHANGED)
if imgPrev is None:
    print("\n\nNo init_" + str(bookid) + ".png")
    print(
        "just use the first picture of the chosen folder, remember the background and save as png\n\n"
    )
    exit()
imgPrevAlpha = imgPrev[:, :, 3]

print(imgPrevAlpha)
imgPrev = cv2.imread(path + book + imgs[0])
monitor2 = 1920

cv2.namedWindow("Matching", cv2.WINDOW_NORMAL)
cv2.resizeWindow("Matching", 700, 500)
cv2.moveWindow("Matching", 0 + monitor2, 0)
if True:
    cv2.namedWindow("Keypoints in new image", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("Keypoints in new image", 700, 500)
    cv2.moveWindow("Keypoints in new image", 700 + monitor2, 0)
if drawDebugImgs:
    cv2.namedWindow("cropped image with keypoints", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("cropped image with keypoints", 700, 500)
    cv2.moveWindow("cropped image with keypoints", 0 + monitor2, 500)
    cv2.namedWindow("Mask", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("Mask", 700, 500)
    cv2.moveWindow("Mask", 700 + monitor2, 500)
    cv2.namedWindow("lines", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("lines", 700, 500)
    cv2.moveWindow("lines", 1400 + monitor2, 500)
cv2.namedWindow("labeled image", cv2.WINDOW_NORMAL)
cv2.resizeWindow("labeled image", 700, 500)
cv2.moveWindow("labeled image", 1400 + monitor2, 0)


method1(imgs, imgPrev, imgPrevAlpha, path, book, bookid)
# cv2.waitKey(0)

# method0(imgs, imgPrev)
# method1(imgs, imgPrev)

cv2.destroyAllWindows()
