import cv2
import numpy as np
from rectify import rectify

window = "disp"
window_out = "dissssp"
disp_type = 0
block_idx_min = 0
block_idx_max = 10
num_disp_min = 0
num_disp_max = 25
img = [[], []]

params = {"blockSize": 5, "numDisparities": 16}


def init_extra_params():
    # block_size = params["blockSize"]
    extra = {
        "minDisparity": 0,
        "P1": 0,
        "P2": 0,
        "uniquenessRatio": 0,
        "speckleWindowSize": 0,
        "speckleRange": 0,
        "disp12MaxDiff": 0,
    }
    params.update(extra)


def update_block_size(block_idx):
    block_size = (block_idx + 2) * 2 + 1
    params["blockSize"] = block_size
    print(f"block size = {block_size}")
    if disp_type == 0:
        stereo_bm()
    elif disp_type == 1:
        stereo_sgbm()


def update_num_disp(num_disp_idx):
    num_disp = (num_disp_idx + 1) * 16
    params["numDisparities"] = num_disp
    print(f"num disp = {num_disp_idx+1}")
    if disp_type == 0:
        stereo_bm()
    elif disp_type == 1:
        stereo_sgbm()


def update_p1(p1_idx):
    if p1_idx == 0:
        p1 = 0
    elif p1_idx == 1:
        # p1 = 2 ** (2 * p1_idx)
        p1 = 8
    params["P1"] = p1 * 3 * params["blockSize"] ** 2
    print(f"P1 = {p1}")
    stereo_sgbm()


def update_p2(p2_idx):
    if p2_idx == 0:
        p2 = 0
    elif p2_idx == 1:
        # p2 = 2 ** (4 * p2_idx)
        p2 = 32
    params["P2"] = p2 * 3 * params["blockSize"] ** 2
    print(f"P2 = {p2}")
    stereo_sgbm()


def update_uniqueness(ur):
    params["uniquenessRatio"] = ur
    print(f"Uniqueness ratio = {ur}")
    stereo_sgbm()


def update_speckle_size(size):
    size = 10 * size
    params["speckleWindowSize"] = size
    print(f"Speckle size = {size}")
    stereo_sgbm()


def update_speckle_range(range):
    if range != 0:
        range = 2 ** range
    params["speckleRange"] = range
    print(f"Speckle range = {range}")
    stereo_sgbm()


def update_max_diff(new):
    params["disp12MaxDiff"] = new
    print(f"Max diff = {new}")
    stereo_sgbm()


def stereo_bm():
    stereo = cv2.StereoBM_create(**params)
    disp = stereo.compute(img[0], img[1])
    disp = disp.copy() / np.max(disp)
    cv2.imshow(window_out, disp)


def stereo_sgbm():
    stereo = cv2.StereoSGBM_create(**params)
    disp = stereo.compute(img[0], img[1])
    disp = disp.copy() / 16.0  # np.max(disp)
    cv2.imshow(window_out, disp)


def disp_tune(filepath, type=0):
    global disp_type
    disp_type = type

    frame = [[], []]
    frame[0] = cv2.imread(filepath + "/left/left_0000.png")
    frame[1] = cv2.imread(filepath + "/right/right_0000.png")

    maps, _, _ = rectify(frame[0].shape[:2], alpha=0)
    rect = [[], []]
    for i in range(len(rect)):
        rect[i] = cv2.remap(frame[i], maps[i][0], maps[i][1], cv2.INTER_CUBIC)

    cv2.namedWindow(window, cv2.WINDOW_AUTOSIZE)

    global img
    if type == 0:
        for i in range(len(img)):
            img[i] = cv2.cvtColor(rect[i], cv2.COLOR_BGR2GRAY)

        stereo_bm()
        while True:
            cv2.createTrackbar(
                "block_size", window, block_idx_min, block_idx_max, update_block_size
            )
            cv2.createTrackbar(
                "num_disp", window, num_disp_min, num_disp_max, update_num_disp
            )
            if cv2.waitKey(0) == 27:
                break
    elif type == 1:
        for i in range(len(img)):
            img[i] = rect[i]
        init_extra_params()
        stereo_sgbm()
        while True:
            cv2.createTrackbar(
                "block_size", window, block_idx_min, block_idx_max, update_block_size
            )
            cv2.createTrackbar(
                "num_disp", window, num_disp_min, num_disp_max, update_num_disp
            )
            cv2.createTrackbar("P1", window, 0, 1, update_p1)
            cv2.createTrackbar("P2", window, 0, 1, update_p2)
            cv2.createTrackbar("uniquenessRatio", window, 0, 10, update_uniqueness)
            cv2.createTrackbar("speckle_size", window, 0, 10, update_speckle_size)
            cv2.createTrackbar("speckle_range", window, 0, 6, update_speckle_range)
            cv2.createTrackbar("disp_12_max_diff", window, 0, 1, update_max_diff)
            if cv2.waitKey(0) == 27:
                break


if __name__ == "__main__":
    filepath = "../data/course/sample_stereo_conveyor_without_occlusions"
    disp_tune(filepath, type=1)
